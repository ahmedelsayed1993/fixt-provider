package com.aait.fixtprovider.Models

import java.io.Serializable

class CarModel :Serializable{
    var id:Int?=null
    var image:String?=null
    var vehicle:String?=null
    var engine:String?=null
    var oil_type:String?=null
    var oil:String?=null
    var viscosity:String?=null
    var chassis_number:String?=null
    var plate_letters:String?=null
    var plate_number:String?=null
    var oil_change_date:String?=null
    var oil_change_km:String?=null
    var date_oil_filter:String?=null
    var change_km_oil:String?=null
    var date_filter_makina:String?=null
    var makina_change_km:String?=null
    var date_filter_air:String?=null
    var air_change_km:String?=null
    var model:String?=null
    var year:String?=null
}