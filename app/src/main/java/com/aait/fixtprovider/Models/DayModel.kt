package com.aait.fixtprovider.Models

import java.io.Serializable

class DayModel:Serializable {
    var id:Int?= null
    var date:String?=null
    var total:Float?=null
    var day:String?=null
    var orders:Int?=null
}