package com.aait.fixtprovider.Models

import java.io.Serializable

class FilterModel:Serializable {
    var id:Int?=null
    var filter:String?=null
    var filter_type:String?=null
    var quantity_filter:String?=null

}