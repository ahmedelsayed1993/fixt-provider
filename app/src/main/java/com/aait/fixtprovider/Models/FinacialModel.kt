package com.aait.fixtprovider.Models

import java.io.Serializable

class FinacialModel:Serializable {
    var id:Int?= null
    var order_id:Int?= null
    var total_order:String?=null
    var commission:String?=null
}