package com.aait.fixtprovider.Models

import java.io.Serializable

class NotificationModel:Serializable {
    var id:Int?=null
    var content:String?=null
    var created:String?=null
}