package com.aait.fixtprovider.Models

import java.io.Serializable

class OilModel:Serializable {
    var id:Int?=null
    var oil_type:String?=null
    var quantity_oile:String?=null
    var viscosity:String?=null
}