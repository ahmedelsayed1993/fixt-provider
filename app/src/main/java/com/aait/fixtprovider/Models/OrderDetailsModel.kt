package com.aait.fixtprovider.Models

import java.io.Serializable

class OrderDetailsModel:Serializable {
    var id:Int?=null
    var time:String?=null
    var date:String?=null
    var address:String?=null
    var order_type:String?=null
    var have_oil:Int?=null
    var number_cans:Int?=null
    var price_cans:Float?=null
    var oil:String?=null
    var oil_id:Int?=null
    var oil_type:String?=null
    var oil_type_id:String?=null
    var km_number:String?=null
    var viscosity:String?=null
    var viscosity_id:Int?=null
    var oil_filter:String?=null
    var price_oil_filter:Float?=null
    var filter_makina:String?=null
    var price_filter_makina:Float?=null
    var filter_air:String?=null
    var price_filter_air:Float?=null
    var wash_type_id:String?=null
    var wash_type:String?=null
    var wash_price:String?=null
    var price_service:Float?=null
    var total:Float?=null
    var lat:String?=null
    var lng:String?=null
    var car_id:Int?=null
    var user_id:Int?=null
    var username:String?=null
    var avatar:String?=null
    var payment:String?=null
    var phone_number:String?=null
}