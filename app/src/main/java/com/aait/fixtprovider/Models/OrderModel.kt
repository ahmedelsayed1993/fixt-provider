package com.aait.fixtprovider.Models

import java.io.Serializable

class OrderModel:Serializable {
    var id:Int?=null
    var avatar:String?=null
    var name:String?=null
    var car:String?=null
    var time:String?=null
    var date:String?=null
    var order_type:String?=null
    var oil_filter:String?=null
    var filter_makina:String?=null
    var filter_air:String?=null
    var payment_type:String?=null
    var available_stock:String?=null
    var status:String?=null

}