package com.aait.fixtprovider.Models

import java.io.Serializable

class OrderResponse:BaseResponse(),Serializable {
    var data:ArrayList<OrderModel>?=null
}