package com.aait.fixtprovider.Models

import java.io.Serializable

class ReverenceResponse:BaseResponse(),Serializable {
    var orders:Int?=null
    var total:Float?=null
    var earnings:String?=null
    var data:ArrayList<DayModel>?=null
}