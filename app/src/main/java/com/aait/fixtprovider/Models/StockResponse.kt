package com.aait.fixtprovider.Models

import java.io.Serializable

class StockResponse:BaseResponse(),Serializable {
    var data:ArrayList<OilModel>?=null
    var makina_filters:ArrayList<FilterModel>?=null
    var oil_filters:ArrayList<FilterModel>?=null
    var air_filters:ArrayList<FilterModel>?=null
}