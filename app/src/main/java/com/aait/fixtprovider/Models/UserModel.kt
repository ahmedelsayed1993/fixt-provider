package com.aait.fixtprovider.Models

import java.io.Serializable

class UserModel :Serializable{
    var id:Int?=null
    var name:String?= null
    var phone:String?=null
    var code:String?=null
    var phone_key:String?=null
    var user_type:String?=null
    var provider_type:String?=null
    var email:String?=null
    var provider_data:String?=null
    var lang:String?=null
    var device_id:String?=null
    var device_type:String?=null
    var avatar:String?=null
    var date:String?=null
    var vehicle_registration:String?=null
    var driving_license:String?=null
    var photo_id:String?=null


}