package com.aait.fixtprovider.Models

import java.io.Serializable

class VehiclesResponse:BaseResponse(),Serializable {
    var data:ArrayList<VehicleModel>?=null
}