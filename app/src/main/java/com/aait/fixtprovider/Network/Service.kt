package com.aait.fixtprovider.Network


import com.aait.fixtprovider.Models.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Query

interface Service {

    @POST("phone-keys")
    fun Keys(@Query("lang") lang:String): Call<ListResponse>

    @POST("sign-up")
    fun SignUp(@Query("user_type") user_type:String,
               @Query("name") name:String,
               @Query("phone") phone:String,
               @Query("phone_key") phone_key:String,
               @Query("email") email:String,
               @Query("password") password:String,
               @Query("device_id") device_id:String,
               @Query("device_type") device_type:String,
               @Query("provider_type") provider_type:String,
               @Query("lang") lang:String):Call<UserResponse>
    @POST("check-code")
    fun CheckCode(@Query("user_id") user_id:Int,
                  @Query("code") code:String,
                  @Query("lang") lang: String):Call<UserResponse>
    @POST("resend-code")
    fun resend(@Query("user_id") user_id:Int,
               @Query("lang") lang:String):Call<BaseResponse>
    @POST("sign-in")
    fun login(@Query("lang") lang:String,
              @Query("phone") phone:String,
              @Query("password") password:String,
              @Query("device_id") device_id:String,
              @Query("device_type") device_type:String):Call<UserResponse>

    @POST("forget-password")
    fun forgotPass(@Query("phone") phone:String,
                   @Query("lang") lang:String):Call<UserResponse>

    @POST("update-password")
    fun updatePass(@Query("lang") lang:String,
                   @Query("user_id") user_id:Int,
                   @Query("password") password:String,
                   @Query("code") code: String):Call<UserResponse>
    @Multipart
    @POST("edit-profile-provider")
    fun AddImages(@Query("lang") lang:String,
                  @Query("user_id") user_id:Int,
                  @Part  vehicle_registration:MultipartBody.Part,
                  @Part driving_license:MultipartBody.Part,
                  @Part photo_id:MultipartBody.Part,
                  @Query("phone") phone: String,
                  @Query("email") email: String):Call<UserResponse>
    @Multipart
    @POST("edit-profile-provider")
    fun AddAvatar(@Query("lang") lang:String,
                  @Query("user_id") user_id:Int,
                  @Part  avatar:MultipartBody.Part):Call<UserResponse>

    @POST("edit-profile")
    fun editProfile(@Query("lang") lang:String,
                    @Query("user_id") user_id:Int,
                    @Query("name") name:String?,
                    @Query("phone") phone:String?,
                    @Query("email") email: String?):Call<UserResponse>

    @POST("reset-password")
    fun resetPassword(@Query("lang") lang:String,
                      @Query("user_id") user_id:Int,
                      @Query("current_password") current_password:String,
                      @Query("password") password:String):Call<BaseResponse>

    @POST("provider-orders")
    fun getOrders(@Query("lang") lang:String,
                  @Query("provider_id") provider_id:Int,
                  @Query("status") status:String):Call<OrderResponse>

    @POST("details-order")
    fun getOrder(@Query("lang") lang:String,
                 @Query("order_id") order_id:Int,
                 @Query("provider_id") provider_id:Int,
                 @Query("action") action:String?):Call<OrderDetailsResponse>

    @POST("update-order-provider")
    fun Accept(@Query("lang") lang:String,
               @Query("order_id") order_id:Int,
               @Query("provider_id") provider_id:Int,
               @Query("status") status:String):Call<OrderDetailsResponse>

    @POST("view-car-details")
    fun getCar(@Query("lang") lang:String,
               @Query("provider_id") provider_id:Int,
               @Query("order_id") order_id:Int,
               @Query("chassis_number") chassis_number:String?,
               @Query("plate_letters") plate_letters:String?,
               @Query("plate_number") plate_number:String?,
               @Query("km_number") km_number:String?):Call<CarDetails>

    @POST("update-order-provider")
    fun finish(@Query("lang") lang:String,
               @Query("provider_id") provider_id:Int,
               @Query("order_id") order_id:Int,
               @Query("status") status:String,
               @Query("wash_type") wash_type:Int?,
               @Query("oil_type_id") oil_type_id:Int?,
               @Query("viscosity_id") viscosity_id:Int?,
               @Query("number_cans") number_cans:String?,
               @Query("oil_id") oil_id:Int?):Call<OrderDetailsResponse>

    @POST("revenues")
    fun getRevenues(@Query("lang") lang: String,
                    @Query("provider_id") provider_id:Int,
                    @Query("date_from") date_from:String,
                    @Query("date_to") date_to:String):Call<ReverenceResponse>

    @POST("about")
    fun About(@Query("lang") lang:String):Call<AboutResponse>
    @POST("contact")
    fun getContact(@Query("lang") lang: String):Call<ContactResponse>

    @POST("log-out")
    fun logOut(@Query("user_id") user_id:Int,
               @Query("device_id") device_id:String,
               @Query("device_type") device_type:String,
               @Query("lang") lang:String):Call<AboutResponse>

    @POST("financial")
    fun getFinacial(@Query("lang") lang: String,
                    @Query("provider_id") provider_id:Int):Call<FinacialsResponse>

    @POST("provider-stock")
    fun getStock(@Query("lang") lang:String,
                 @Query("provider_id") provider_id:Int):Call<StockResponse>

    @POST("payment-all-financial")
    fun payAll(@Query("lang") lang: String,
               @Query("provider_id") provider_id:Int):Call<AboutResponse>
    @POST("payment-financial")
    fun pay(@Query("lang") lang: String,
            @Query("provider_id") provider_id:Int,
            @Query("financial_id") financial_id:Int):Call<AboutResponse>

    @POST("notifications")
    fun getNotification(@Query("user_id") user_id: Int,
                        @Query("lang") lang:String):Call<NotificationResponse>
    @POST("delete-notification")
    fun delete(@Query("lang") lang: String,
               @Query("user_id") user_id:Int,
               @Query("notification_id") notification_id:Int):Call<AboutResponse>

    @POST("types-washing")
    fun getWash_types(@Query("lang") lang:String):Call<VehiclesResponse>
    @POST("oils")
    fun getOils(@Query("lang") lang: String):Call<VehiclesResponse>
    @POST("oil-types")
    fun getOilTypes(@Query("lang") lang: String):Call<VehiclesResponse>

    @POST("viscosities")
    fun getViscosity(@Query("lang") lang:String,
                     @Query("type_id") type_id:Int):Call<VehiclesResponse>
}