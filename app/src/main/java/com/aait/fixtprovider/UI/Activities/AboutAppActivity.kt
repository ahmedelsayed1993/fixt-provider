package com.aait.fixtprovider.UI.Activities

import android.content.Intent
import android.widget.ImageView
import android.widget.TextView
import com.aait.fixtprovider.Base.Parent_Activity
import com.aait.fixtprovider.Client
import com.aait.fixtprovider.Models.AboutResponse
import com.aait.fixtprovider.Network.Service
import com.aait.fixtprovider.R
import com.aait.fixtprovider.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AboutAppActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_about_app
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var about_app:TextView

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        about_app = findViewById(R.id.about_app)
        back.setOnClickListener{startActivity(Intent(this@AboutAppActivity,MainActivity::class.java))
        finish()}
        title.text = getString(R.string.about_app)
        getAbout()

    }
    fun getAbout(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.About(mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<AboutResponse> {
            override fun onFailure(call: Call<AboutResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<AboutResponse>, response: Response<AboutResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        about_app.text = response.body()?.data
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        about_app.text = getString(R.string.content_not_found_you_can_still_search_the_app_freely)
                    }
                }
            }

        })
    }
    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this,MainActivity::class.java))
    }
}