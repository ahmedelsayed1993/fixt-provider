package com.aait.fixtprovider.UI.Activities

import android.content.Intent
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.fixtprovider.Base.Parent_Activity
import com.aait.fixtprovider.Client
import com.aait.fixtprovider.Models.BaseResponse
import com.aait.fixtprovider.Models.UserModel
import com.aait.fixtprovider.Models.UserResponse
import com.aait.fixtprovider.Network.Service
import com.aait.fixtprovider.R
import com.aait.fixtprovider.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ActivateAccountActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_activate_account
    lateinit var back:ImageView
    lateinit var code:EditText
    lateinit var confirm:Button
    lateinit var resend:TextView
    lateinit var userModel: UserModel
    override fun initializeComponents() {
        userModel = intent.getSerializableExtra("user") as UserModel
        back = findViewById(R.id.back)
        code = findViewById(R.id.code)
        confirm = findViewById(R.id.confirm)
        resend = findViewById(R.id.resend)
        confirm.setOnClickListener { if (CommonUtil.checkEditError(code,getString(R.string.activation_code))){
        return@setOnClickListener}else{
            check()
        }
        }
        back.setOnClickListener { onBackPressed()
        finish()}
        resend.setOnClickListener {  resend()}

    }
    fun check(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.CheckCode(userModel.id!!,code.text.toString(),mLanguagePrefManager.appLanguage)
            ?.enqueue(object : Callback<UserResponse> {
                override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<UserResponse>,
                    response: Response<UserResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
//                            mSharedPrefManager.loginStatus = true
//                            mSharedPrefManager.userData = response.body()?.data!!
                            startActivity(Intent(this@ActivateAccountActivity,LoginActivity::class.java))
                            finish()
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
    }

    fun resend(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.resend(userModel.id!!,mLanguagePrefManager.appLanguage)?.enqueue(
            object :Callback<BaseResponse>{
                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<BaseResponse>,
                    response: Response<BaseResponse>
                ) {
                  hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            }
        )
    }
}