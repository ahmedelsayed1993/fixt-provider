package com.aait.fixtprovider.UI.Activities

import android.content.Intent
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.fixtprovider.Base.Parent_Activity
import com.aait.fixtprovider.Client
import com.aait.fixtprovider.Models.CarDetails
import com.aait.fixtprovider.Models.CarModel
import com.aait.fixtprovider.Network.Service
import com.aait.fixtprovider.R
import com.aait.fixtprovider.Uitls.CommonUtil
import com.bumptech.glide.Glide
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CarDetailsActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_car_details
    var id = 0
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var image:ImageView
    lateinit var type:TextView
    lateinit var make_year:TextView
    lateinit var model:TextView
    lateinit var capacity:TextView
    lateinit var vin_no:EditText
    lateinit var plate_letter:EditText
    lateinit var plate_number:EditText
    lateinit var km_num:EditText
    lateinit var confirm:Button
    lateinit var oil_type:TextView
    lateinit var viscosity:TextView
    lateinit var oil:TextView
    lateinit var oil_filter:TextView
    lateinit var makena_filter:TextView
    lateinit var air_filter:TextView
    var f_oil = ""
    var f_makena = ""
    var f_air = ""

    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        f_oil = intent.getStringExtra("oil")
        f_makena = intent.getStringExtra("makena")
        f_air = intent.getStringExtra("air")
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        image = findViewById(R.id.image)
        type = findViewById(R.id.type)
        make_year = findViewById(R.id.make_year)
        model = findViewById(R.id.model)
        capacity = findViewById(R.id.capacity)
        vin_no = findViewById(R.id.vin_no)
        plate_number = findViewById(R.id.plate_number)
        plate_letter = findViewById(R.id.plate_letter)
        km_num = findViewById(R.id.km_num)
        confirm = findViewById(R.id.confirm)
        oil_type = findViewById(R.id.oil_type)
        viscosity = findViewById(R.id.viscosity)
        oil = findViewById(R.id.oil)
        oil_filter = findViewById(R.id.oil_filter)
        makena_filter = findViewById(R.id.makena_filter)
        air_filter = findViewById(R.id.air_filter)
        back.setOnClickListener { startActivity(Intent(this@CarDetailsActivity,MainActivity::class.java))
        finish()}
        title.text = getString(R.string.order_details_number)+" "+id
        getCar()

        confirm.setOnClickListener { if (CommonUtil.checkEditError(vin_no,getString(R.string.Vin_No))||
            CommonUtil.checkString(vin_no,"")||
                CommonUtil.checkEditError(plate_letter,getString(R.string.plate_letters))||
                CommonUtil.checkEditError(plate_number,getString(R.string.plate_number))||
                CommonUtil.checkEditError(km_num,getString(R.string.km_num))){
        return@setOnClickListener
        }else{
            update()
        }
        }

    }
    fun setData(carModel: CarModel){
        plate_letter.setText(carModel.plate_letters!!.replace(""," "))
        plate_number.setText(carModel.plate_number)
        vin_no.setText(carModel.chassis_number)
        Glide.with(mContext).load(carModel.image).into(image)
        if(carModel.vehicle.equals("")){
            type.text = getString(R.string.undefined)
        }else {
            type.text = carModel.vehicle
        }
        if (carModel.viscosity.equals("")){
            viscosity.text = getString(R.string.undefined)
        }else {
            viscosity.text = carModel.viscosity
        }
        if (carModel.oil.equals("")){
            oil.text = getString(R.string.undefined)
        }else {
            oil.text = carModel.oil
        }
        if (carModel.model.equals("")){
            model.text = getString(R.string.undefined)
        }else {
            model.text = carModel.model
        }
        if (carModel.engine.equals("")){
            capacity.text = getString(R.string.undefined)
        }else {
            capacity.text = carModel.engine
        }
        if (carModel.year.equals("")){
            make_year.text = getString(R.string.undefined)
        }
        else {
            make_year.text = carModel.year
        }
        if (carModel.oil_type.equals("")){
            oil_type.text = getString(R.string.undefined)
        }else {
            oil_type.text = carModel.oil_type
        }
        if (f_oil.equals("")){
            oil_filter.text = getString(R.string.undefined)
        }else {
            oil_filter.text = f_oil
//            if (f_oil.equals("original")){
//                oil_filter.text = getString(R.string.original)
//            }else{
//                oil_filter.text = getString(R.string.agency)
//            }

        }
        if (f_air.equals("")){
            air_filter.text = getString(R.string.undefined)
        }else {
            air_filter.text = f_air
//            if (f_air.equals("original")){
//                air_filter.text = getString(R.string.original)
//            }else{
//                air_filter.text = getString(R.string.agency)
//            }
        }
        if (f_makena.equals("")){
            makena_filter.text = getString(R.string.undefined)
        }else {
            makena_filter.text = f_makena
//            if (f_makena.equals("original")){
//                makena_filter.text = getString(R.string.original)
//            }else{
//                makena_filter.text = getString(R.string.agency)
//            }
        }
    }

    fun getCar(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getCar(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,id,null,null,null,null)
            ?.enqueue(object : Callback<CarDetails>{
                override fun onFailure(call: Call<CarDetails>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(call: Call<CarDetails>, response: Response<CarDetails>) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            setData(response.body()?.data!!)

                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
    }

    fun update(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getCar(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!
            ,id,vin_no.text.toString(),plate_letter.text.toString(),plate_number.text.toString(),km_num.text.toString())?.enqueue(
            object :Callback<CarDetails>{
                override fun onFailure(call: Call<CarDetails>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(call: Call<CarDetails>, response: Response<CarDetails>) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            val intent = Intent(this@CarDetailsActivity, FinishOrderActivity::class.java)
                            intent.putExtra("id",id)
                            startActivity(intent)
                            finish()

                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            }
        )
    }
}