package com.aait.fixtprovider.UI.Activities

import android.content.Intent
import android.os.Build
import android.util.Log
import android.util.TimeUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.aait.fixtprovider.Base.Parent_Activity
import com.aait.fixtprovider.Client
import com.aait.fixtprovider.Models.DayModel
import com.aait.fixtprovider.Models.ReverenceResponse
import com.aait.fixtprovider.Network.Service
import com.aait.fixtprovider.R
import com.aait.fixtprovider.Uitls.CommonUtil
import com.aminography.primecalendar.PrimeCalendar
import com.aminography.primecalendar.civil.CivilCalendar
import com.aminography.primecalendar.common.CalendarFactory
import com.aminography.primecalendar.common.CalendarType
import com.aminography.primedatepicker.PickType

import com.aminography.primedatepicker.fragment.PrimeDatePickerBottomSheet
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import java.text.SimpleDateFormat
import java.time.Duration
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList






class ChartActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_earnings
    lateinit var barChart: BarChart
    lateinit var date:TextView
    var start:String? = null
    var end:String? = null
    lateinit var earn:TextView
    lateinit var earning:TextView
    lateinit var coast:TextView
    lateinit var order_num:TextView
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var date1:Date
    lateinit var date2:Date
    var difference = 0
    var days = 0


    var i = 0
    override fun initializeComponents() {
        barChart = findViewById(R.id.barChart)
        date = findViewById(R.id.date)
        earn = findViewById(R.id.earn)
        earning = findViewById(R.id.earnings)
        coast = findViewById(R.id.coast)
        order_num = findViewById(R.id.num_orders)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)

        back.setOnClickListener { startActivity(Intent(this@ChartActivity,MainActivity::class.java))
        finish()}
        title.text = getString(R.string.Revenues)


        var calendar = CalendarFactory.newInstance(CalendarType.CIVIL)
       // calendar.locale = Locale.getDefault()
        date.setOnClickListener {
            val datePicker = PrimeDatePickerBottomSheet.newInstance(
                CivilCalendar() ,
                null,
                calendar, // can be null
                PickType.RANGE_START, // can be null
                null, // can be null
                null, // can be null
                null, // can be null
                null, // can be null
                null // can be null
            )

            datePicker.setOnDateSetListener(object : PrimeDatePickerBottomSheet.OnDayPickedListener {
                override fun onMultipleDaysPicked(multipleDays: List<PrimeCalendar>) {

                }

                @RequiresApi(Build.VERSION_CODES.O)
                override fun onRangeDaysPicked(startDay: PrimeCalendar, endDay: PrimeCalendar) {


                    var spf = SimpleDateFormat("dd-MM-yyyy", Locale("en"))
                    date1 = startDay.getTime()
                    date2 = endDay.getTime()
                    date.text = startDay.monthDayString+" - "+endDay.monthDayString

                    start = spf.format(date1)
                    end = spf.format(date2)
                    Log.e("start",start)
                    Log.e("end",end)
                    difference = (date2.time - date1.time).toInt()

                    days = TimeUnit.MILLISECONDS.toDays(difference.toLong()).toInt()
                    Log.e("diff", days.toString())
                    if (days==7){
                        getChart()
                    }else{
                        CommonUtil.makeToast(mContext,getString(R.string.select_days))
                    }
                }

                override fun onSingleDayPicked(singleDay: PrimeCalendar) {

                }


            })

            datePicker.show(supportFragmentManager, "SOME_TAG")


        }

            val datePicker = PrimeDatePickerBottomSheet.newInstance(
                CivilCalendar() ,
                null,
                calendar, // can be null
                PickType.RANGE_START, // can be null
                null, // can be null
                null, // can be null
                null, // can be null
                null, // can be null
                null // can be null
            )



            datePicker.setOnDateSetListener(object : PrimeDatePickerBottomSheet.OnDayPickedListener {
                override fun onMultipleDaysPicked(multipleDays: List<PrimeCalendar>) {

                }

                @RequiresApi(Build.VERSION_CODES.O)
                override fun onRangeDaysPicked(startDay: PrimeCalendar, endDay: PrimeCalendar) {


                   var spf = SimpleDateFormat("dd-MM-yyyy", Locale("en"))
                    date1 = startDay.getTime()
                    Log.e("date",date1.toString())
                    date2 = endDay.getTime()
                    Log.e("date",date2.toString())
                    date.text = startDay.monthDayString+" - "+endDay.monthDayString

                    start = spf.format(date1)
                    end = spf.format(date2)
                    Log.e("start",start)
                    Log.e("end",end)
                    difference = (date2.time - date1.time).toInt()

                    days = TimeUnit.MILLISECONDS.toDays(difference.toLong()).toInt()
                    Log.e("diff", days.toString())
                    if (days==7){
                        getChart()
                    }else{
                        CommonUtil.makeToast(mContext,getString(R.string.select_days))
                    }



                }

                override fun onSingleDayPicked(singleDay: PrimeCalendar) {

                }


            })

            datePicker.show(supportFragmentManager, "SOME_TAG")



    }

    private fun setBarChart(days:ArrayList<DayModel>) {
        val entries = ArrayList<BarEntry>()
        for (i in days){

            entries.add(BarEntry(i.total!!,i.id!!))
        }
//        entries.add(BarEntry(8f, 0))
//        entries.add(BarEntry(2f, 1))
//        entries.add(BarEntry(5f, 2))
//        entries.add(BarEntry(20f, 3))
//        entries.add(BarEntry(15f, 4))
//        entries.add(BarEntry(19f, 5))

        val barDataSet = BarDataSet(entries, "")

        val labels = ArrayList<String>()
        for (i in days){

            labels.add(i.day!!)
        }
//        labels.add("18-Jan")
//        labels.add("19-Jan")
//        labels.add("20-Jan")
//        labels.add("21-Jan")
//        labels.add("22-Jan")
//        labels.add("23-Jan")
        val data = BarData(labels,barDataSet)
        barChart.data = data // set the data and list of lables into chart

        barChart.setDescription("")  // set the description

        //barDataSet.setColors(ColorTemplate.COLORFUL_COLORS)
        barDataSet.color = resources.getColor(R.color.colorPrimaryDark)

        barChart.animateY(5000)
    }

    fun getChart(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getRevenues(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,start!!,end!!)
            ?.enqueue(object : Callback<ReverenceResponse> {
                override fun onFailure(call: Call<ReverenceResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<ReverenceResponse>,
                    response: Response<ReverenceResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            setBarChart(response.body()?.data!!)
                            coast.text = getString(R.string.SR)+" "+response.body()?.total.toString()
                            earn.text = getString(R.string.SR)+" "+response.body()?.earnings.toString()
                            earning.text = getString(R.string.SR)+" "+response.body()?.earnings.toString()
                            order_num.text = response.body()?.orders.toString()
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }

                }

            })
    }
    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this,MainActivity::class.java))
    }
}


