package com.aait.fixtprovider.UI.Activities

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import androidx.core.app.ActivityCompat
import com.aait.fixtprovider.Base.Parent_Activity
import com.aait.fixtprovider.Client
import com.aait.fixtprovider.Models.ContactResponse
import com.aait.fixtprovider.Models.SocialModel
import com.aait.fixtprovider.Network.Service
import com.aait.fixtprovider.R
import com.aait.fixtprovider.Uitls.CommonUtil
import com.aait.fixtprovider.Uitls.PermissionUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ContactUsActivity :Parent_Activity(){
    override val layoutResource: Int
        get() = R.layout.activity_contact_us
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var phone:TextView


    lateinit var twitter:ImageView
    lateinit var insta:ImageView
    lateinit var face:ImageView
    var f = ""
    var i = ""
    var t = ""
    var s = ""
    var socials = ArrayList<SocialModel>()
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        phone = findViewById(R.id.phone)

        twitter = findViewById(R.id.twitter)
        insta = findViewById(R.id.insta)
        face = findViewById(R.id.face)
        back.setOnClickListener { startActivity(Intent(this@ContactUsActivity,MainActivity::class.java))
        finish()}
        title.text = getString(R.string.contat_us)
        contact()
        phone.setOnClickListener { getLocationWithPermission(phone.text.toString()) }

        insta.setOnClickListener {
            if (!i.equals("")) {
                if (i.startsWith("http")) {
                    Log.e("here", "333")
                    val i_ = Intent(Intent.ACTION_VIEW)
                    i_.data = Uri.parse(i)
                    startActivity(i_)

                } else {
                    Log.e("here", "4444")
                    val url = "https://$i"
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(url)
                    startActivity(i)
                }
            }
        }
        twitter.setOnClickListener {
            if (!t.equals("")) {
                if (t.startsWith("http")) {
                    Log.e("here", "333")
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(t)
                    startActivity(i)

                } else {
                    Log.e("here", "4444")
                    val url = "https://$t"
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(url)
                    startActivity(i)
                }
            }
        }
        face.setOnClickListener {
            getLocationWithPermission(f)
        }


    }

    internal fun callnumber(number: String) {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$number")
        mContext.startActivity(intent)
    }

    fun getLocationWithPermission(number: String) {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(mContext, Manifest.permission.CALL_PHONE)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    ActivityCompat.requestPermissions(
                        mContext as Activity, PermissionUtils.CALL_PHONE,
                        300
                    )
            } else {
                callnumber(number)
            }
        } else {
            callnumber(number)
        }

    }
    fun contact(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getContact(mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<ContactResponse> {
            override fun onFailure(call: Call<ContactResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(
                call: Call<ContactResponse>,
                response: Response<ContactResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        phone.text = response.body()?.phones?.phone

                        socials = response.body()?.data!!
                       f = socials.get(0).link!!
                        t = socials.get(1).link!!
                        i = socials.get(2).link!!

                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this,MainActivity::class.java))
    }
}