package com.aait.fixtprovider.UI.Activities

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.aait.fixtprovider.Base.Parent_Activity
import com.aait.fixtprovider.Client
import com.aait.fixtprovider.Models.UserResponse
import com.aait.fixtprovider.Network.Service
import com.aait.fixtprovider.R
import com.aait.fixtprovider.Uitls.CommonUtil
import com.aait.fixtprovider.Uitls.PermissionUtils
import com.bumptech.glide.Glide
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import com.jaiselrahman.filepicker.activity.FilePickerActivity
import de.hdodenhof.circleimageview.CircleImageView
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.util.ArrayList

class EditProfileActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_edit_data
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var image:CircleImageView
    lateinit var name:TextView
    lateinit var phone:EditText
    lateinit var email:EditText
    lateinit var vehicle_form:ImageView
    lateinit var driving_licences:ImageView
    lateinit var identity:ImageView
    lateinit var confirm:Button
    private var ImageBasePath: String? = null
    private var ImageBasePath1: String? = null
    private var ImageBasePath2: String? = null
    private var ImageBasePath3: String? = null
    internal var returnValue: ArrayList<String>? = ArrayList()
    internal var options = Options.init()
        .setRequestCode(100)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
    internal var returnValue1: ArrayList<String>? = ArrayList()
    internal var options1 = Options.init()
        .setRequestCode(200)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue1)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
    internal var returnValue2: ArrayList<String>? = ArrayList()
    internal var options2 = Options.init()
        .setRequestCode(300)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue2)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
    internal var returnValue3: ArrayList<String>? = ArrayList()
    internal var options3 = Options.init()
        .setRequestCode(400)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue3)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        image = findViewById(R.id.image)
        name = findViewById(R.id.name)
        phone = findViewById(R.id.phone)
        email = findViewById(R.id.email)
        vehicle_form = findViewById(R.id.vehicle_form)
        driving_licences = findViewById(R.id.driving_licences)
        identity = findViewById(R.id.identity)
        confirm = findViewById(R.id.confirm)
        title.text = getString(R.string.edit_my_data)
        back.setOnClickListener { startActivity(Intent(this@EditProfileActivity,MainActivity::class.java))
        finish()}
        Glide.with(mContext).load(mSharedPrefManager.userData.avatar).into(image)
        name.text = mSharedPrefManager.userData.name
        phone.setText(mSharedPrefManager.userData.phone)
        email.setText(mSharedPrefManager.userData.email)

        confirm.setOnClickListener {
            if (ImageBasePath1!=null){
                if (ImageBasePath2!=null){
                    if (ImageBasePath3!=null){
                        if (CommonUtil.checkEditError(phone,getString(R.string.phone_number))||
                                CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                                CommonUtil.checkEditError(email,getString(R.string.email))||
                               ! CommonUtil.isEmailValid(email,getString(R.string.correct_email))){
                            return@setOnClickListener
                        }else{
                            addAvater(ImageBasePath1!!,ImageBasePath2!!,ImageBasePath3!!)
                        }
                    }else{
                        CommonUtil.makeToast(mContext,getString(R.string.A_copy_of_identity_or_residency))
                    }
                }else{
                    CommonUtil.makeToast(mContext,getString(R.string.driving_license))
                }
            }else{
                CommonUtil.makeToast(mContext,getString(R.string.Copy_of_the_vehicle_form))
            }
        }

        image.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            PermissionUtils.IMAGE_PERMISSIONS,
                            400
                        )
                    }
                } else {
                    Pix.start(this, options)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options)
            }
        }
        vehicle_form.setOnClickListener {  if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext,
                    android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        )) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.IMAGE_PERMISSIONS,
                        400
                    )
                }
            } else {
                Pix.start(this, options1)
                CommonUtil.PrintLogE("Permission is granted before")
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23")
            Pix.start(this, options1)
        } }
        driving_licences.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            PermissionUtils.IMAGE_PERMISSIONS,
                            400
                        )
                    }
                } else {
                    Pix.start(this, options2)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options2)
            }
        }
        identity.setOnClickListener {  if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext,
                    android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        )) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.IMAGE_PERMISSIONS,
                        400
                    )
                }
            } else {
                Pix.start(this, options3)
                CommonUtil.PrintLogE("Permission is granted before")
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23")
            Pix.start(this, options3)
        } }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
          if (requestCode == 100) {
            returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

            ImageBasePath = returnValue!![0]
            // Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload).fitCenter().sizeMultiplier(100)).into(profile_image);
            image!!.setImageURI(Uri.parse(ImageBasePath))
            if (ImageBasePath != null) {

                addAvater(ImageBasePath!!)
            }
        }else if (requestCode == 200){
              returnValue1 = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

              ImageBasePath1 = returnValue1!![0]
              // Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload).fitCenter().sizeMultiplier(100)).into(profile_image);
              vehicle_form!!.setImageURI(Uri.parse(ImageBasePath1))
              if (ImageBasePath1 != null) {
//                updateImage(ImageBasePath!!)
              }
          }else if (requestCode == 300){
              returnValue2 = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

              ImageBasePath2 = returnValue2!![0]
              // Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload).fitCenter().sizeMultiplier(100)).into(profile_image);
              driving_licences!!.setImageURI(Uri.parse(ImageBasePath2))
              if (ImageBasePath2 != null) {
//                updateImage(ImageBasePath!!)
              }
          }else if (requestCode == 400){
              returnValue3 = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

              ImageBasePath3 = returnValue3!![0]
              // Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload).fitCenter().sizeMultiplier(100)).into(profile_image);
              identity!!.setImageURI(Uri.parse(ImageBasePath3))
              if (ImageBasePath3 != null) {
//                updateImage(ImageBasePath!!)
              }
          }
    }

    fun addAvater(path:String){
        showProgressDialog(getString(R.string.please_wait))
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.name, fileBody)
        Client.getClient()?.create(Service::class.java)?.AddAvatar(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,filePart)?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                 CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                hideProgressDialog()


            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        mSharedPrefManager.userData = response.body()?.data!!
                        Glide.with(mContext).load(response.body()?.data?.avatar).into(image)
                        Glide.with(mContext).load(response.body()?.data?.driving_license).into(driving_licences)
                        Glide.with(mContext).load(response.body()?.data?.vehicle_registration).into(vehicle_form)
                        Glide.with(mContext).load(response.body()?.data?.photo_id).into(identity)
                        name.text = mSharedPrefManager.userData.name
                        phone.setText(mSharedPrefManager.userData.phone)
                        email.setText(mSharedPrefManager.userData.email)

                    }else{
                         CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun addAvater(path:String,path1:String,path2:String){
        showProgressDialog(getString(R.string.please_wait))
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("vehicle_registration", ImageFile.name, fileBody)
        var filePart1: MultipartBody.Part? = null
        val ImageFile1 = File(path1)
        val fileBody1 = RequestBody.create(MediaType.parse("*/*"), ImageFile1)
        filePart1 = MultipartBody.Part.createFormData("driving_license", ImageFile.name, fileBody1)
        var filePart2: MultipartBody.Part? = null
        val ImageFile2 = File(path2)
        val fileBody2 = RequestBody.create(MediaType.parse("*/*"), ImageFile2)
        filePart2 = MultipartBody.Part.createFormData("photo_id", ImageFile.name, fileBody)
        Client.getClient()?.create(Service::class.java)?.AddImages(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,filePart,filePart1,filePart2,phone.text.toString(),email.text.toString())?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                 CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                hideProgressDialog()


            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        mSharedPrefManager.userData = response.body()?.data!!
                        Glide.with(mContext).load(response.body()?.data?.avatar).into(image)
                        Glide.with(mContext).load(response.body()?.data?.driving_license).into(driving_licences)
                        Glide.with(mContext).load(response.body()?.data?.vehicle_registration).into(vehicle_form)
                        Glide.with(mContext).load(response.body()?.data?.photo_id).into(identity)
                        name.text = mSharedPrefManager.userData.name
                        phone.setText(mSharedPrefManager.userData.phone)
                        email.setText(mSharedPrefManager.userData.email)

                    }else{
                         CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}