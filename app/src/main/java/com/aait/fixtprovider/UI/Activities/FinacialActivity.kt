package com.aait.fixtprovider.UI.Activities

import android.app.Dialog
import android.content.Intent
import android.text.InputType
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.fixtprovider.Base.Parent_Activity
import com.aait.fixtprovider.Client
import com.aait.fixtprovider.Listeners.OnItemClickListener
import com.aait.fixtprovider.Models.*
import com.aait.fixtprovider.Network.Service
import com.aait.fixtprovider.R
import com.aait.fixtprovider.UI.Adapters.FinicialAdapter
import com.aait.fixtprovider.UI.Adapters.OrderAdapter
import com.aait.fixtprovider.Uitls.CommonUtil
import io.fotoapparat.selector.autoRedEye
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FinacialActivity :Parent_Activity(),OnItemClickListener{
    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.settlement){
           // pay(orderModels.get(position).id!!)
            val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.dialog_pay)

            val cash = dialog?.findViewById<RadioButton>(R.id.cash)
            val online = dialog?.findViewById<RadioButton>(R.id.online)
            val confirm = dialog?.findViewById<Button>(R.id.confirm)
            var payment = "online"
            cash?.setOnClickListener { payment = "cash" }
            online?.setOnClickListener { payment = "online" }

            confirm?.setOnClickListener {
                if (payment == "cash"){
                    pay(orderModels.get(position).id!!)
                }else{
                    val intent = Intent(this,PayOneActivity::class.java)
                    intent.putExtra("order",orderModels.get(position).id!!)
                    startActivity(intent)
                    dialog?.dismiss()

                }

            }
            dialog?.show()
            // pay()


        }

    }

    override val layoutResource: Int
        get() = R.layout.activity_finicials
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var orders:TextView
    lateinit var amount:TextView
    lateinit var pay:Button
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null
    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    internal lateinit var linearLayoutManager: LinearLayoutManager
    internal var orderModels = java.util.ArrayList<FinacialModel>()
    internal lateinit var orderAdapter: FinicialAdapter

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        orders = findViewById(R.id.orders)
        amount = findViewById(R.id.amount)
        pay = findViewById(R.id.pay)
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        orderAdapter =  FinicialAdapter(mContext,orderModels,R.layout.recycler_finacial)
        orderAdapter.setOnItemClickListener(this)
        rv_recycle!!.layoutManager= linearLayoutManager
        rv_recycle!!.adapter = orderAdapter
        title.text = getString(R.string.Financial_accounts)
        back.setOnClickListener { startActivity(Intent(this@FinacialActivity,MainActivity::class.java))
        finish()}
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener { getFinacial() }
        getFinacial()

        pay.setOnClickListener {
            val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.dialog_pay)

            val cash = dialog?.findViewById<RadioButton>(R.id.cash)
            val online = dialog?.findViewById<RadioButton>(R.id.online)
            val confirm = dialog?.findViewById<Button>(R.id.confirm)
            var payment = "online"
            cash?.setOnClickListener { payment = "cash" }
            online?.setOnClickListener { payment = "online" }

            confirm?.setOnClickListener {
                if (payment == "cash"){
                    pay()
                }else{
                    startActivity(Intent(this,PayActivity::class.java))
                    dialog?.dismiss()
                }

            }
            dialog?.show()
           // pay()
        }


    }

    fun getFinacial(){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.getFinacial(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!)?.enqueue(object :
            Callback<FinacialsResponse> {
            override fun onFailure(call: Call<FinacialsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
            }

            override fun onResponse(
                call: Call<FinacialsResponse>,
                response: Response<FinacialsResponse>
            ) {
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){

                        amount.text = response.body()?.amount.toString()
                        orders.text = response.body()?.total_order.toString()
                        if (response.body()?.amount == 0F){
                            pay.visibility = View.GONE
                        }else{
                            pay.visibility = View.VISIBLE
                        }
                        if (response.body()?.data?.isEmpty()!!){
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                        }else{
                            orderAdapter.updateAll(response.body()?.data!!)
                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    fun pay(id:Int){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.pay(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,id)?.enqueue(object :Callback<AboutResponse>{
            override fun onFailure(call: Call<AboutResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<AboutResponse>, response: Response<AboutResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.data!!)
                        startActivity(Intent(this@FinacialActivity,MainActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun pay(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.payAll(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!)?.enqueue(object :Callback<AboutResponse>{
            override fun onFailure(call: Call<AboutResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<AboutResponse>, response: Response<AboutResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.data!!)
                        startActivity(Intent(this@FinacialActivity,MainActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this,MainActivity::class.java))
    }

}