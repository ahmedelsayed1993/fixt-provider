package com.aait.fixtprovider.UI.Activities

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import com.aait.fixtprovider.Base.Parent_Activity
import com.aait.fixtprovider.Client
import com.aait.fixtprovider.GPS.GPSTracker
import com.aait.fixtprovider.GPS.GpsTrakerListener
import com.aait.fixtprovider.Listeners.OnItemClickListener
import com.aait.fixtprovider.Models.OrderDetailsModel
import com.aait.fixtprovider.Models.OrderDetailsResponse
import com.aait.fixtprovider.Models.VehicleModel
import com.aait.fixtprovider.Models.VehiclesResponse
import com.aait.fixtprovider.Network.Service
import com.aait.fixtprovider.R
import com.aait.fixtprovider.UI.Views.ListDialog
import com.aait.fixtprovider.Uitls.CommonUtil
import com.aait.fixtprovider.Uitls.DialogUtil
import com.aait.fixtprovider.Uitls.PermissionUtils
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.roundToInt

class FinishOrderActivity : Parent_Activity(),OnItemClickListener , GpsTrakerListener {
    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // Log.e("lat",lat.toString()+" "+log.toString())
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }
    override fun onItemClick(view: View, position: Int) {
        if (selected==0){
            listDialog.dismiss()
            oilModel = oils.get(position)
            oil.text = oilModel.name
            Accept(null,null,null,oilModel.id,null)
//            update(oilModel.id,null,null,null,null,null,null)
        }else if (selected ==1){
            listDialog.dismiss()
            typeModel1 = types1.get(position)
            type.text = typeModel1.name
            id_ = typeModel1.id!!
            Accept(typeModel1.id)
//            update(null,typeModel.id,null,null,null,null,null)
        }else if (selected == 2){
            listDialog.dismiss()
            viscosotyModel = viscosities.get(position)
            viscosity.text = viscosotyModel.name
            Accept(null,null,viscosotyModel.id,null,null)
//            update(null,null,viscosotyModel.id,null,null,null,null)
        }else if (selected == 3){
            listDialog.dismiss()
            typeModel = types.get(position)
            wash_type.text = typeModel.name
            Accept(typeModel.id,null,null,null,null)

//            update(null,null,null,oilfilter.id,null,null,null)
        }
    }

    override val layoutResource: Int
        get() = R.layout.activity_finish
    lateinit var oil_change:TextView
    lateinit var type_change:TextView
    lateinit var visc_change:TextView
    lateinit var wash_change:TextView
    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var image: CircleImageView
    lateinit var name: TextView
    lateinit var address: TextView
    lateinit var date: TextView
    lateinit var car_details:TextView
    private lateinit var Oil_lay: LinearLayout
    lateinit var oil: TextView
    lateinit var can_lay: LinearLayout
    lateinit var can_num: EditText
    lateinit var type_lay: LinearLayout
    lateinit var type: TextView
    lateinit var vis_lay: LinearLayout
    lateinit var viscosity: TextView
    lateinit var oil_lay: LinearLayout
    lateinit var oil_filter: TextView
    lateinit var makena_lay: LinearLayout
    lateinit var Makina_filter: TextView
    lateinit var air_lay: LinearLayout
    lateinit var air_filter: TextView
    lateinit var amount: TextView
    lateinit var phone:ImageView
    var telephone = ""
    lateinit var pay: TextView
    lateinit var accept: Button
    lateinit var wash_lay: LinearLayout
    lateinit var wash_type: TextView
    lateinit var can_change:TextView
    var lat = ""
    var lng = ""
    lateinit var listDialog: ListDialog
    lateinit var typeModel: VehicleModel
    var types = ArrayList<VehicleModel>()
    var oils = ArrayList<VehicleModel>()
    var types1 = ArrayList<VehicleModel>()
    var viscosities = ArrayList<VehicleModel>()
    lateinit var oilModel: VehicleModel
    lateinit var typeModel1: VehicleModel
    lateinit var viscosotyModel: VehicleModel
    var id = 0
    var id_ = 0
    var selected = 0
    lateinit var orderDetailsModel: OrderDetailsModel
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""
    private var mAlertDialog: AlertDialog? = null
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        oil_change = findViewById(R.id.oil_change)
        type_change = findViewById(R.id.type_change)
        visc_change = findViewById(R.id.visc_change)
        wash_change = findViewById(R.id.wash_change)
        can_change = findViewById(R.id.can_change)
        phone = findViewById(R.id.phone)
        title.text = getString(R.string.order_details_number)+" "+id
        back.setOnClickListener { startActivity(Intent(this@FinishOrderActivity,MainActivity::class.java))
            finish()}
        image = findViewById(R.id.image)
        name = findViewById(R.id.name)
        address = findViewById(R.id.address)
        date = findViewById(R.id.date)
        oil_lay = findViewById(R.id.oil_lay)
        oil = findViewById(R.id.oil)
        can_lay = findViewById(R.id.can_lay)
        can_num = findViewById(R.id.can_num)
        Oil_lay = findViewById(R.id.Oil_lay)
        type_lay = findViewById(R.id.type_lay)
        type = findViewById(R.id.type)
        vis_lay = findViewById(R.id.vis_lay)
        viscosity = findViewById(R.id.viscosity)
        oil_filter = findViewById(R.id.oil_filter)
        makena_lay = findViewById(R.id.makena_lay)
        Makina_filter = findViewById(R.id.Makina_filter)
        air_lay = findViewById(R.id.air_lay)
        air_filter = findViewById(R.id.air_filter)
        amount = findViewById(R.id.amount)
        pay = findViewById(R.id.pay)
        accept = findViewById(R.id.accept)

        wash_lay = findViewById(R.id.wash_lay)
        wash_type = findViewById(R.id.wash_type)
        getLocationWithPermission()
        getOrder()
        address.setOnClickListener { startActivity(
            Intent(
                Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?saddr=" + mLat + "," + mLang + "&daddr=" + lat + "," + lng)
            )
        )}
        can_change.setOnClickListener { can_num.requestFocus()
            val imm = mContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(can_num, InputMethodManager.SHOW_IMPLICIT)}
        accept.setOnClickListener { Accept() }
        oil_change.setOnClickListener { selected = 0
        getOil()}
        type_change.setOnClickListener { selected = 1
        getTypes1()}
        visc_change.setOnClickListener { selected = 2
        getViscosity(id_)}
        wash_change.setOnClickListener { selected = 3
        getTypes()}
        phone.setOnClickListener {
            if (telephone.equals("")){

            }else{
                getLocationWithPermission(telephone) }
        }
        car_details = findViewById(R.id.car_details)
        car_details.setOnClickListener {
            val intent = Intent(this,CarDetailsActivity::class.java)
            intent.putExtra("id",id)
            intent.putExtra("oil",orderDetailsModel.oil_filter)
            intent.putExtra("makena",orderDetailsModel.filter_makina)
            intent.putExtra("air",orderDetailsModel.filter_air)
            startActivity(intent)
            finish()
        }
        can_num.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView, actionId: Int, event: KeyEvent?): Boolean {
                if (event != null && event!!.getKeyCode() === KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE) {
                    if (can_num.text.toString().equals("")){

                    }else {
                        Accept(null,null,null,null,can_num.text.toString().toFloat().roundToInt().toString())
                    }
                }
                return false
            }
        })



    }
    internal fun callnumber(number: String) {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$number")
        mContext.startActivity(intent)
    }

    fun getLocationWithPermission(number: String) {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(mContext, Manifest.permission.CALL_PHONE)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    ActivityCompat.requestPermissions(
                        mContext as Activity, PermissionUtils.CALL_PHONE,
                        300
                    )
            } else {
                callnumber(number)
            }
        } else {
            callnumber(number)
        }



    }


    fun setData(orderDetailsModel: OrderDetailsModel){
        Glide.with(mContext).load(orderDetailsModel.avatar).into(image)
        name.text = orderDetailsModel.username
        telephone = orderDetailsModel.phone_number!!
        date.text = orderDetailsModel.date+" - "+orderDetailsModel.time
        amount.text = orderDetailsModel.total.toString()+getString(R.string.SR)
        if (orderDetailsModel.payment.equals("")){
            pay.text = getString(R.string.Not_paid)
        }else{
            if (orderDetailsModel.payment.equals("cash")){
                pay.text = getString(R.string.pay_cash)
            }else {
                pay.text = getString(R.string.pay_online)
            }
        }

        lat = orderDetailsModel.lat!!
        lng = orderDetailsModel.lng!!
        val content = SpannableString(orderDetailsModel.address)
        content.setSpan(UnderlineSpan(), 0, content.length, 0)
        address.setText(content)

        if (orderDetailsModel.wash_type.equals("")){
            wash_lay.visibility = View.GONE
        }else{
            wash_lay.visibility = View.VISIBLE
            wash_type.text = orderDetailsModel.wash_type
        }
        if (orderDetailsModel.oil.equals("")){
            Oil_lay.visibility = View.GONE
        }else{
            Oil_lay.visibility = View.VISIBLE
            oil.text = orderDetailsModel.oil
        }
        if (orderDetailsModel.oil_type.equals("")){
            type_lay.visibility = View.GONE
            id_ = 0
        }else{
            type_lay.visibility = View.VISIBLE
            type.text = orderDetailsModel.oil_type
            id_ = orderDetailsModel.oil_type_id!!.toInt()
        }
        if (orderDetailsModel.number_cans==0){
            can_lay.visibility = View.GONE
        }else{
            can_lay.visibility = View.VISIBLE
            can_num.setText(orderDetailsModel.number_cans.toString())
        }
        if (orderDetailsModel.viscosity.equals("")){
            vis_lay.visibility = View.GONE
        }else{
            vis_lay.visibility = View.VISIBLE
            viscosity.text = orderDetailsModel.viscosity
        }
        if (orderDetailsModel.oil_filter.equals("")){
            oil_lay.visibility = View.GONE
        }else{
            oil_lay.visibility = View.VISIBLE
            oil_filter.text = orderDetailsModel.oil_filter
//            if (orderDetailsModel.oil_filter.equals("original")){
//                oil_filter.text = getString(R.string.original)
//            }else {
//                oil_filter.text = getString(R.string.agency)
//            }
        }
        if (orderDetailsModel.filter_makina.equals("")){
            makena_lay.visibility = View.GONE
        }else{
            makena_lay.visibility = View.VISIBLE
            Makina_filter.text = orderDetailsModel.filter_makina
//            if (orderDetailsModel.filter_makina.equals("original")){
//                Makina_filter.text = getString(R.string.original)
//            }else{
//                Makina_filter.text = getString(R.string.agency)
//            }

        }
        if (orderDetailsModel.filter_air.equals("")){
            air_lay.visibility = View.GONE
        }else{
            air_lay.visibility = View.VISIBLE
            air_filter.text = orderDetailsModel.filter_air
//            if (orderDetailsModel.filter_air.equals("original")){
//                air_filter.text = getString(R.string.original)
//            }else{
//                air_filter.text = getString(R.string.agency)
//            }
        }

    }

    fun getOrder(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getOrder(mLanguagePrefManager.appLanguage,id,mSharedPrefManager.userData.id!!
            ,null)?.enqueue(object : Callback<OrderDetailsResponse> {
            override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<OrderDetailsResponse>,
                response: Response<OrderDetailsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        orderDetailsModel = response.body()?.data!!
                        setData(response.body()?.data!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }


    fun Accept(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.finish(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,id
            ,"completed",null,null,null,null,null)?.enqueue(object : Callback<OrderDetailsResponse> {
            override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<OrderDetailsResponse>,
                response: Response<OrderDetailsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        val intent = Intent(this@FinishOrderActivity,MainActivity::class.java)
                        intent.putExtra("id",id)
                        startActivity(intent)
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }

    fun Accept(wash:Int?,type:Int?,vis:Int?,oil:Int?,num:String?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.finish(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,id
            ,"completed",wash,type,vis,num,oil)?.enqueue(object : Callback<OrderDetailsResponse> {
            override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<OrderDetailsResponse>,
                response: Response<OrderDetailsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        setData(response.body()?.data!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }
    fun Accept(type:Int?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.finish(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,id
            ,"completed",null,type,null,null,null)?.enqueue(object : Callback<OrderDetailsResponse> {
            override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<OrderDetailsResponse>,
                response: Response<OrderDetailsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        setData(response.body()?.data!!)
                        selected = 2
                        getViscosity(id_)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }

    fun getTypes(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getWash_types(mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<VehiclesResponse> {
            override fun onFailure(call: Call<VehiclesResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<VehiclesResponse>,
                response: Response<VehiclesResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        types = response.body()?.data!!
                        listDialog = ListDialog(mContext,this@FinishOrderActivity,types,getString(R.string.Washing_type))
                        listDialog.show()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun getOil(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getOils(mLanguagePrefManager.appLanguage)?.enqueue(object:
            Callback<VehiclesResponse> {
            override fun onFailure(call: Call<VehiclesResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<VehiclesResponse>,
                response: Response<VehiclesResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        oils = response.body()?.data!!
                        listDialog = ListDialog(mContext,this@FinishOrderActivity,oils,getString(R.string.oil))
                        listDialog.show()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun getTypes1(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getOilTypes(mLanguagePrefManager.appLanguage)?.enqueue(object:
            Callback<VehiclesResponse> {
            override fun onFailure(call: Call<VehiclesResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                 hideProgressDialog()
            }

            override fun onResponse(
                call: Call<VehiclesResponse>,
                response: Response<VehiclesResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        types1 = response.body()?.data!!
                        listDialog = ListDialog(mContext,this@FinishOrderActivity,types1,getString(R.string.Oil_type))
                        listDialog.show()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun getViscosity(id:Int){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getViscosity(mLanguagePrefManager.appLanguage,id)?.enqueue(object:
            Callback<VehiclesResponse> {
            override fun onFailure(call: Call<VehiclesResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<VehiclesResponse>,
                response: Response<VehiclesResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        viscosities = response.body()?.data!!
                        listDialog = ListDialog(mContext,this@FinishOrderActivity,viscosities,getString(R.string.Degree_of_viscosity))
                        listDialog.show()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun getLocationWithPermission() {
        gps = GPSTracker(mContext, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                        (PermissionUtils.hasPermissions(mContext,
                            Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.GPS_PERMISSION,
                        800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation()
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation()
        }

    }

    internal fun getCurrentLocation() {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext,
                getString(R.string.gps_detecting),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    mAlertDialog?.dismiss()
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(intent, 300)
                })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(this, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                        java.lang.Double.parseDouble(mLat),
                        java.lang.Double.parseDouble(mLang),
                        1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                            mContext,
                            resources.getString(R.string.detect_location),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)





                    }


                } catch (e: IOException) {
                }



            }
        }
    }


}