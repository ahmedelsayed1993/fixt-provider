package com.aait.fixtprovider.UI.Activities

import android.content.Intent
import android.view.View
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.TextView
import com.aait.fixtprovider.Base.Parent_Activity
import com.aait.fixtprovider.R


class LanguageActivity :Parent_Activity(){
    lateinit var arabic:RadioButton
    lateinit var english:RadioButton
    lateinit var back:ImageView
    lateinit var title:TextView

    override val layoutResource: Int
        get() = R.layout.activity_app_language

    override fun initializeComponents() {


        arabic = findViewById(R.id.arabic)
        english = findViewById(R.id.english)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)

        title.visibility =View.VISIBLE

        title.text = getString(R.string.app_language)
        back.setOnClickListener { startActivity(Intent(this@LanguageActivity,MainActivity::class.java))
        finish()}
        if (mLanguagePrefManager.appLanguage.equals("ar")){
            arabic.isChecked = true
        }else{
            english.isChecked = true
        }
        arabic.setOnClickListener { mLanguagePrefManager.appLanguage = "ar"
            startActivity(Intent(this@LanguageActivity,SplashActivity::class.java))
           this@LanguageActivity.finish()
        }
        english.setOnClickListener { mLanguagePrefManager.appLanguage = "en"
            startActivity(Intent(this@LanguageActivity,SplashActivity::class.java))
            this@LanguageActivity.finish()
        }


    }
    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this,MainActivity::class.java))
    }
}