package com.aait.fixtprovider.UI.Activities

import android.content.Intent
import android.graphics.Color
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.GravityCompat
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.aait.fixtprovider.Base.Parent_Activity
import com.aait.fixtprovider.Client
import com.aait.fixtprovider.Models.AboutResponse
import com.aait.fixtprovider.Models.FinacialsResponse
import com.aait.fixtprovider.Models.UserResponse
import com.aait.fixtprovider.Network.Service
import com.aait.fixtprovider.R
import com.aait.fixtprovider.UI.Fragments.HomeFragment
import com.aait.fixtprovider.UI.Fragments.OrderFragment
import com.aait.fixtprovider.UI.Fragments.ProfileFragment
import com.aait.fixtprovider.Uitls.CommonUtil
import com.bumptech.glide.Glide
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_main.*
import me.ibrahimsn.lib.OnItemSelectedListener
import me.ibrahimsn.lib.SmoothBottomBar
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity :Parent_Activity(){
    override val layoutResource: Int
        get() = R.layout.activity_main
    lateinit var bottomBar: SmoothBottomBar
    private var fragmentManager: FragmentManager? = null

    private var transaction: FragmentTransaction? = null
    internal lateinit var homeFragment: HomeFragment
    internal lateinit var profileFragment: ProfileFragment
    internal lateinit var orderFragment: OrderFragment

    lateinit var menu: ImageView
    lateinit var image: CircleImageView
    lateinit var name: TextView
    lateinit var main: TextView
    lateinit var Revenues: TextView
    lateinit var profile: TextView
    lateinit var Inventory: TextView
    lateinit var contact_us: TextView
    lateinit var about_app: TextView
    lateinit var logout: TextView
    lateinit var title:TextView
    lateinit var logo:ImageView
    lateinit var financial:TextView
    lateinit var notification:ImageView
    lateinit var lang:TextView
    lateinit var photo:TextView
    var deviceID=""
    override fun initializeComponents() {
        deviceID = FirebaseInstanceId.getInstance().token.toString()
        title = findViewById(R.id.title)
        logo = findViewById(R.id.logo)
        menu = findViewById(R.id.menu)
        bottomBar = findViewById(R.id.bottomBar)
        notification = findViewById(R.id.notification)
        notification.setOnClickListener { startActivity(Intent(this@MainActivity,NotificationActivity::class.java))
        finish()}
        homeFragment = HomeFragment()
        profileFragment = ProfileFragment()
        orderFragment = OrderFragment()
        fragmentManager = getSupportFragmentManager()
        transaction = fragmentManager!!.beginTransaction()
        transaction!!.add(R.id.home_fragment_container, profileFragment)
        transaction!!.add(R.id.home_fragment_container, orderFragment)
        transaction!!.add(R.id.home_fragment_container, homeFragment)

        transaction!!.commit()
        bottomBar.setOnItemSelectedListener(object :OnItemSelectedListener{
            override fun onItemSelect(pos: Int) {
                if (pos==0){
                    transaction = fragmentManager!!.beginTransaction()
                    //        transaction.hide(mMoreFragment);
                    //        transaction.hide(mOrdersFragment);
                    //        transaction.hide(mFavouriteFragment);
                    transaction!!.replace(R.id.home_fragment_container, profileFragment)
                    transaction!!.commit()
                }else if (pos==1){
                    transaction = fragmentManager!!.beginTransaction()
                    //        transaction.hide(mMoreFragment);
                    //        transaction.hide(mOrdersFragment);
                    //        transaction.hide(mFavouriteFragment);
                    transaction!!.replace(R.id.home_fragment_container, orderFragment)
                    transaction!!.commit()
                }else if (pos==2){
                    transaction = fragmentManager!!.beginTransaction()
                    //        transaction.hide(mMoreFragment);
                    //        transaction.hide(mOrdersFragment);
                    //        transaction.hide(mFavouriteFragment);
                    transaction!!.replace(R.id.home_fragment_container, homeFragment)
                    transaction!!.commit()
                }
            }

        })
        getProfile()
        sideMenu()
        getFinacial()
        title.setOnClickListener {
            startActivity(Intent(this@MainActivity,ChartActivity::class.java))
            finish()

        }

    }

    fun sideMenu(){
        drawer_layout.useCustomBehavior(Gravity.START)
        drawer_layout.useCustomBehavior(Gravity.END)
        drawer_layout.setRadius(Gravity.START, 25f)
        drawer_layout.setRadius(Gravity.END, 25f)
        drawer_layout.setViewScale(Gravity.START, 0.9f) //set height scale for main view (0f to 1f)
        drawer_layout.setViewScale(Gravity.END, 0.9f) //set height scale for main view (0f to 1f)
        drawer_layout.setViewElevation(Gravity.START, 20f) //set main view elevation when drawer open (dimension)
        drawer_layout.setViewElevation(Gravity.END, 20f) //set main view elevation when drawer open (dimension)
        drawer_layout.setViewScrimColor(Gravity.START, Color.TRANSPARENT) //set drawer overlay coloe (color)
        drawer_layout.setViewScrimColor(Gravity.END, Color.TRANSPARENT) //set drawer overlay coloe (color)
        drawer_layout.setDrawerElevation(Gravity.START, 20f) //set drawer elevation (dimension)
        drawer_layout.setDrawerElevation(Gravity.END, 20f) //set drawer elevation (dimension)
        drawer_layout.setContrastThreshold(3f) //set maximum of contrast ratio between white text and background color.
        drawer_layout.setRadius(Gravity.START, 40f) //set end container's corner radius (dimension)

        drawer_layout.setRadius(Gravity.END, 40f)

        menu.setOnClickListener { drawer_layout.openDrawer(GravityCompat.START) }

        logout = drawer_layout.findViewById(R.id.logout)
        image = drawer_layout.findViewById(R.id.image)
        name = drawer_layout.findViewById(R.id.name)
        photo = drawer_layout.findViewById(R.id.photo)
        main = drawer_layout.findViewById(R.id.main)
        about_app = drawer_layout.findViewById(R.id.about_app)
        financial = drawer_layout.findViewById(R.id.financial)
        Inventory = drawer_layout.findViewById(R.id.Inventory)
        profile = drawer_layout.findViewById(R.id.profile)
        Revenues = drawer_layout.findViewById(R.id.Revenues)
        lang = drawer_layout.findViewById(R.id.lang)
        contact_us = drawer_layout.findViewById(R.id.contact_us)
        Log.e("user",Gson().toJson(mSharedPrefManager.userData))
        if (mSharedPrefManager.userData.avatar!!.isEmpty()){
            photo.visibility = View.VISIBLE
            image.visibility = View.GONE
            var st = mSharedPrefManager.userData.name
            photo.text = st?.get(0).toString().capitalize()
            Log.e("char", st?.get(0).toString().capitalize())
        }else{
            photo.visibility = View.GONE
            image.visibility = View.VISIBLE
            Glide.with(mContext).load(mSharedPrefManager.userData.avatar).into(image)
        }


        name.text = mSharedPrefManager.userData.name


        main.setOnClickListener { drawer_layout.closeDrawer(GravityCompat.START) }
        profile.setOnClickListener {
            drawer_layout.closeDrawer(GravityCompat.START)
            transaction = fragmentManager!!.beginTransaction()
            //        transaction.hide(mMoreFragment);
            //        transaction.hide(mOrdersFragment);
            //        transaction.hide(mFavouriteFragment);
            transaction!!.replace(R.id.home_fragment_container, profileFragment)
            transaction!!.commit()
        bottomBar.setActiveItem(0)}
        financial.setOnClickListener { startActivity(Intent(this@MainActivity,FinacialActivity::class.java))
        finish()}
        Revenues.setOnClickListener { startActivity(Intent(this@MainActivity,ChartActivity::class.java))
          finish()  }
        about_app.setOnClickListener { startActivity(Intent(this@MainActivity,AboutAppActivity::class.java))
          finish()  }
        Inventory.setOnClickListener { startActivity(Intent(this@MainActivity,StockActivity::class.java))
         finish()   }
        contact_us.setOnClickListener { startActivity(Intent(this@MainActivity,ContactUsActivity::class.java))
           finish() }
        lang.setOnClickListener { startActivity(Intent(this@MainActivity,LanguageActivity::class.java))
         finish()   }

//        about_app.setOnClickListener { startActivity(Intent(this@ProviderMainActivity,AboutAppActivity::class.java)) }
//        terms.setOnClickListener { startActivity(Intent(this@ProviderMainActivity,TermsActivity::class.java))  }
//        repeated.setOnClickListener { startActivity(Intent(this@ProviderMainActivity,RepeatedQuestionsActivity::class.java)) }
//        contact_us.setOnClickListener { startActivity(Intent(this@ProviderMainActivity,ContactUsActivity::class.java)) }
//        complains.setOnClickListener { startActivity(Intent(this@ProviderMainActivity,ComplainsActivity::class.java)) }
//        share_app.setOnClickListener { CommonUtil.ShareApp(applicationContext) }
//        Financial_accounts.setOnClickListener { startActivity(Intent(this@ProviderMainActivity,FinancialActivity::class.java)) }
//
        logout.setOnClickListener { logout() }
    }


    fun logout(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.logOut(mSharedPrefManager.userData.id!!,mSharedPrefManager.userData.device_id!!,"android",mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<AboutResponse> {
            override fun onFailure(call: Call<AboutResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<AboutResponse>,
                response: Response<AboutResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        mSharedPrefManager.loginStatus=false
                        mSharedPrefManager.Logout()
                        CommonUtil.makeToast(mContext,response.body()?.data!!)
                        startActivity(Intent(this@MainActivity, SplashActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }

    fun getFinacial(){

        Client.getClient()?.create(Service::class.java)?.getFinacial(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!)
            ?.enqueue(object:Callback<FinacialsResponse>{
                override fun onFailure(call: Call<FinacialsResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()

                }

                override fun onResponse(
                    call: Call<FinacialsResponse>,
                    response: Response<FinacialsResponse>
                ) {

                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            if (response.body()?.amount?.equals(0.0)!!){
                                logo.visibility = View.VISIBLE
                                title.visibility = View.GONE

                            }else{
                                logo.visibility = View.GONE
                                title.visibility = View.VISIBLE
                                title.text = response.body()?.amount!!.toString()+" "+getString(R.string.SR)
                            }
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
    }
    override fun onBackPressed() {
        super.onBackPressed()
        this@MainActivity.finish()
    }
    fun getProfile(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.editProfile(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!
            ,null,null,null)?.enqueue(object : Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                 CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){

                        mSharedPrefManager.userData = response.body()?.data!!
                    }else{
                         CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}