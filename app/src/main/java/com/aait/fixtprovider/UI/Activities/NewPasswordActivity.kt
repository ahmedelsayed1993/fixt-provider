package com.aait.fixtprovider.UI.Activities

import android.content.Intent
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.fixtprovider.Base.Parent_Activity
import com.aait.fixtprovider.Client
import com.aait.fixtprovider.Models.BaseResponse
import com.aait.fixtprovider.Models.UserModel
import com.aait.fixtprovider.Models.UserResponse
import com.aait.fixtprovider.Network.Service
import com.aait.fixtprovider.R
import com.aait.fixtprovider.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NewPasswordActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_new_password
    lateinit var back:ImageView
    lateinit var code:EditText
    lateinit var password:EditText
    lateinit var confirm_pass:EditText
    lateinit var confirm:Button
    lateinit var resend:TextView
    lateinit var userModel: UserModel

    override fun initializeComponents() {
        userModel = intent.getSerializableExtra("user") as UserModel
        back = findViewById(R.id.back)
        code = findViewById(R.id.code)
        password = findViewById(R.id.new_password)
        confirm_pass = findViewById(R.id.confirm_new_pass)
        confirm = findViewById(R.id.confirm)
        resend = findViewById(R.id.resend)
        back.setOnClickListener { onBackPressed()
        finish()}

        resend.setOnClickListener { resend() }
        confirm.setOnClickListener { if (CommonUtil.checkEditError(code,getString(R.string.activation_code))||
                CommonUtil.checkEditError(password,getString(R.string.new_password))||
                CommonUtil.checkLength(password,getString(R.string.password_length),6)||
                CommonUtil.checkEditError(confirm_pass,getString(R.string.confirm_new_password))){
        return@setOnClickListener}else{
            if (!password.text.toString().equals(confirm_pass.text.toString())){
                CommonUtil.makeToast(mContext,getString(R.string.password_not_match))
            }else{
            updatePass()
            }
        }
        }


    }

    fun updatePass(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.updatePass(mLanguagePrefManager.appLanguage,userModel.id!!,password.text.toString(),code.text.toString())?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
               hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        startActivity(Intent(this@NewPasswordActivity,LoginActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    fun resend(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.resend(userModel.id!!,mLanguagePrefManager.appLanguage)?.enqueue(
            object :Callback<BaseResponse>{
                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<BaseResponse>,
                    response: Response<BaseResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            }
        )
    }
}