package com.aait.fixtprovider.UI.Activities

import android.content.Intent
import android.view.View
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.fixtprovider.Base.Parent_Activity
import com.aait.fixtprovider.Client
import com.aait.fixtprovider.Listeners.OnItemClickListener
import com.aait.fixtprovider.Models.*
import com.aait.fixtprovider.Network.Service
import com.aait.fixtprovider.R

import com.aait.fixtprovider.UI.Adapters.NotificationAdapter
import com.aait.fixtprovider.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NotificationActivity:Parent_Activity() ,OnItemClickListener{
    override fun onItemClick(view: View, position: Int) {
        if (view.id==R.id.delete){
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.delete(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!
                ,notificationModels.get(position).id!!)?.enqueue(object :Callback<AboutResponse>{
                override fun onFailure(call: Call<AboutResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<AboutResponse>,
                    response: Response<AboutResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            CommonUtil.makeToast(mContext,response.body()?.data!!)
                            getCars()
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
        }

    }

    override val layoutResource: Int
        get() = R.layout.activity_notification
    lateinit var back: ImageView
    lateinit var title: TextView

    lateinit var rv_recycle: RecyclerView

    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    internal var swipeRefresh: SwipeRefreshLayout? = null
    internal var notificationModels= ArrayList<NotificationModel>()
    internal lateinit var notificationAdapter: NotificationAdapter
    lateinit var linearLayoutManager: LinearLayoutManager

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoItem = findViewById(R.id.lay_no_item)
        layNoInternet = findViewById(R.id.lay_no_internet)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL,false)
        notificationAdapter = NotificationAdapter(mContext,notificationModels,R.layout.recycler_notification)
        notificationAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = notificationAdapter
        back.setOnClickListener { startActivity(Intent(this@NotificationActivity,MainActivity::class.java))
            finish()}
        title.text = getString(R.string.notifications)
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener { getCars() }
        getCars()

    }

    fun getCars(){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.getNotification(mSharedPrefManager.userData.id!!,mLanguagePrefManager.appLanguage)?.enqueue(
            object : Callback<NotificationResponse> {
                override fun onFailure(call: Call<NotificationResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    layNoInternet!!.visibility = View.VISIBLE
                    layNoItem!!.visibility = View.GONE
                    swipeRefresh!!.isRefreshing = false
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<NotificationResponse>,
                    response: Response<NotificationResponse>
                ) {
                    hideProgressDialog()
                    swipeRefresh!!.isRefreshing = false
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            if (response.body()?.data?.isEmpty()!!){
                                layNoItem!!.visibility = View.VISIBLE
                                layNoInternet!!.visibility = View.GONE
                                tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                            }else{

                                notificationAdapter.updateAll(response.body()?.data!!)
                            }
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            }
        )
    }
    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this,MainActivity::class.java))
    }
}