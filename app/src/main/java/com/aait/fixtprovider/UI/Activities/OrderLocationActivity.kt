package com.aait.fixtprovider.UI.Activities

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.aait.fixtprovider.Base.Parent_Activity
import com.aait.fixtprovider.GPS.GPSTracker
import com.aait.fixtprovider.GPS.GpsTrakerListener
import com.aait.fixtprovider.R
import com.aait.fixtprovider.Uitls.CommonUtil
import com.aait.fixtprovider.Uitls.PermissionUtils
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import java.io.IOException
import java.util.*

class OrderLocationActivity :Parent_Activity(), OnMapReadyCallback
     {
    override fun onMapReady(p0: GoogleMap?) {
        this.googleMap = p0!!

        getLocationWithPermission()
    }



//    override fun onTrackerSuccess(lat: Double?, log: Double?) {
//        if (startTracker) {
//            if (lat != 0.0 && log != 0.0) {
//                hideProgressDialog()
//                putMapMarker(lat, log)
//            }
//        }    }
//
//    override fun onStartTracker() {
//        startTracker = true
//    }

    override val layoutResource: Int
        get() = R.layout.activity_order_location
    internal lateinit var googleMap: GoogleMap
    internal lateinit var myMarker: Marker
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    lateinit var back:ImageView
    lateinit var your_location:TextView
    lateinit var map:MapView
    var lat = ""
    var lng = ""
    var address = ""
    var type = ""

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        your_location = findViewById(R.id.your_location)
        map = findViewById(R.id.map)
        lat = intent.getStringExtra("lat")
        lng = intent.getStringExtra("lng")
        address = intent.getStringExtra("address")
        type = intent.getStringExtra("type")
        back.setOnClickListener { onBackPressed()
        finish()}
        map.onCreate(mSavedInstanceState)
        map.onResume()
        map.getMapAsync(this)

        try {
            MapsInitializer.initialize(mContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        your_location.text = address

    }
         fun getLocationWithPermission() {

             if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                 if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                             (PermissionUtils.hasPermissions(mContext,
                                 Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                     CommonUtil.PrintLogE("Permission not granted")
                     if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                         requestPermissions(
                             PermissionUtils.GPS_PERMISSION,
                             800
                         )
                         Log.e("GPS", "1")
                     }
                 } else {
                     putMapMarker(lat.toDouble(),lng.toDouble())
                     Log.e("GPS", "2")
                 }
             } else {
                 Log.e("GPS", "3")
                 putMapMarker(lat.toDouble(),lng.toDouble())
             }

         }

//         internal fun getCurrentLocation() {
//             gps.getLocation()
//             if (!gps.canGetLocation()) {
//                 mAlertDialog = DialogUtil.showAlertDialog(mContext,
//                     getString(R.string.gps_detecting),
//                     DialogInterface.OnClickListener { dialogInterface, i ->
//                         mAlertDialog?.dismiss()
//                         val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
//                         startActivityForResult(intent, 300)
//                     })
//             } else {
//                 if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
//                     putMapMarker(gps.getLatitude(), gps.getLongitude())
//                     mLat = gps.getLatitude().toString()
//                     mLang = gps.getLongitude().toString()
//                     val addresses: List<Address>
//                     geocoder = Geocoder(this, Locale.getDefault())
//                     try {
//                         addresses = geocoder.getFromLocation(
//                             java.lang.Double.parseDouble(mLat),
//                             java.lang.Double.parseDouble(mLang),
//                             1
//                         )
//                         if (addresses.isEmpty()) {
//                             Toast.makeText(
//                                 mContext,
//                                 resources.getString(R.string.detect_location),
//                                 Toast.LENGTH_SHORT
//                             ).show()
//                         } else {
//                             your_location.text = addresses[0].getAddressLine(0)
//
//
//                         }
//
//                     } catch (e: IOException) {
//                     }
//
//                     googleMap.clear()
//                     putMapMarker(gps.getLatitude(), gps.getLongitude())
//
//                 }
//             }
//         }

         fun putMapMarker(lat: Double?, log: Double?) {
             val latLng = LatLng(lat!!, log!!)
             if (type.equals("wash")) {
                 myMarker = googleMap.addMarker(
                     MarkerOptions()
                         .position(latLng)
                         .title("موقعي")
                         .icon(BitmapDescriptorFactory.fromResource(R.mipmap.washlocation))
                 )
             }else if (type.equals("oil")){
                 myMarker = googleMap.addMarker(MarkerOptions()
                         .position(latLng)
                         .title("موقعي")
                         .icon(BitmapDescriptorFactory.fromResource(R.mipmap.oillocation)))
             }
             googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10f))
             // kkjgj

         }
}