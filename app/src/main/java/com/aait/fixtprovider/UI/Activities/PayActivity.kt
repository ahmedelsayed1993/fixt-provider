package com.aait.fixtprovider.UI.Activities

import android.content.Intent
import android.webkit.WebView
import android.widget.ImageView
import android.widget.TextView
import com.aait.fixtprovider.Base.Parent_Activity
import com.aait.fixtprovider.R

class PayActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_pay

    lateinit var title:TextView
    lateinit var back:ImageView
    lateinit var web:WebView

    override fun initializeComponents() {

        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        web = findViewById(R.id.web)
        title.text = getString(R.string.pay_online)
        back.setOnClickListener { onBackPressed()
        finish()}
        web.settings.javaScriptEnabled = true
        web.loadUrl("https://zaiit.aait-sa.com/online-payment?financial=all&provider="+mSharedPrefManager.userData.id)



    }
}