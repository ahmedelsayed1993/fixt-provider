package com.aait.fixtprovider.UI.Activities

import android.content.Intent
import android.webkit.WebView
import android.widget.ImageView
import android.widget.TextView
import com.aait.fixtprovider.Base.Parent_Activity
import com.aait.fixtprovider.R

class PayOneActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_pay

    lateinit var title:TextView
    lateinit var back:ImageView
    lateinit var web:WebView
    var order = 0
    override fun initializeComponents() {
        order = intent.getIntExtra("order",0)
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        web = findViewById(R.id.web)
        title.text = getString(R.string.pay_online)
        back.setOnClickListener { onBackPressed()
        finish()}
        web.settings.javaScriptEnabled = true
        web.loadUrl("https://zaiit.aait-sa.com/online-payment?financial=one"+"&financial_id="+order+"&provider="+mSharedPrefManager.userData.id)



    }
}