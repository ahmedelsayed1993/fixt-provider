package com.aait.fixtprovider.UI.Activities

import android.content.Intent
import android.net.Uri
import android.widget.Button
import android.widget.LinearLayout
import com.aait.fixtprovider.Base.Parent_Activity
import com.aait.fixtprovider.R

class PreRegisterActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_pre_register
    lateinit var client:Button
    lateinit var provider:Button
    lateinit var login:LinearLayout

    override fun initializeComponents() {
        client = findViewById(R.id.client)
        provider = findViewById(R.id.provider)
        login = findViewById(R.id.login)
        client.setOnClickListener { startActivity(Intent(this@PreRegisterActivity,RegisterActivity::class.java))
        }
        provider.setOnClickListener {  startActivity(
            Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://play.google.com/store/apps/details?id=com.aait.fixt")
            )
        ) }
        login.setOnClickListener { startActivity(Intent(this@PreRegisterActivity,LoginActivity::class.java))
        finish()}

    }
}