package com.aait.fixtprovider.UI.Activities

import android.content.Intent
import android.view.View
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.fixtprovider.Base.Parent_Activity
import com.aait.fixtprovider.Client
import com.aait.fixtprovider.Listeners.OnItemClickListener
import com.aait.fixtprovider.Models.ListModel
import com.aait.fixtprovider.Models.ListResponse
import com.aait.fixtprovider.Models.UserResponse
import com.aait.fixtprovider.Network.Service
import com.aait.fixtprovider.UI.Adapters.ListAdapter
import com.aait.fixtprovider.R
import com.aait.fixtprovider.Uitls.CommonUtil
import com.google.firebase.iid.FirebaseInstanceId
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import java.util.ArrayList

class RegisterActivity:Parent_Activity() ,OnItemClickListener{
    override fun onItemClick(view: View, position: Int) {
        listModel = listModels.get(position)
        code.text = listModel.code
        codes.visibility = View.GONE

    }

    override val layoutResource: Int
        get() = R.layout.activity_register
    lateinit var back:ImageView
    lateinit var register:Button
    lateinit var phone:EditText
    lateinit var name:EditText
    lateinit var code:TextView
    lateinit var emai:EditText
    lateinit var password:EditText
    lateinit var terms:CheckBox
    lateinit var login:LinearLayout
    lateinit var type:TextView
    lateinit var types:CardView
    lateinit var oil:TextView
    lateinit var car:TextView
    lateinit var codes:RecyclerView
    internal lateinit var linearLayoutManager: LinearLayoutManager
    internal var listModels = ArrayList<ListModel>()
    internal lateinit var listAdapter:ListAdapter
    internal lateinit var listModel:ListModel
    var deviceID=""
    var Type = ""
    var num = ""

    override fun initializeComponents() {
        deviceID = FirebaseInstanceId.getInstance().token.toString()
        back = findViewById(R.id.back)
        register = findViewById(R.id.register)
        phone = findViewById(R.id.phone)
        name = findViewById(R.id.name)
        code = findViewById(R.id.code)
        emai = findViewById(R.id.email)
        password = findViewById(R.id.password)
        terms = findViewById(R.id.terms)
        login = findViewById(R.id.login)
        type = findViewById(R.id.type)
        types = findViewById(R.id.types)
        oil = findViewById(R.id.oil)
        car = findViewById(R.id.car)
        codes = findViewById(R.id.codes)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        listAdapter = ListAdapter(mContext,listModels,R.layout.recycler_list)
        listAdapter.setOnItemClickListener(this)
        codes.layoutManager = linearLayoutManager
        codes.adapter = listAdapter
        login.setOnClickListener { startActivity(Intent(this@RegisterActivity,LoginActivity::class.java))
        finish()}
        back.setOnClickListener { onBackPressed()
        finish()}
        terms.setOnClickListener {  }
        register.setOnClickListener { if (CommonUtil.checkEditError(name,getString(R.string.name))||
                CommonUtil.checkEditError(phone,getString(R.string.phone_number))||
                CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                CommonUtil.checkTextError(code,getString(R.string.key))||
                CommonUtil.checkEditError(emai,getString(R.string.email))||
                !CommonUtil.isEmailValid(emai,getString(R.string.correct_email))||
                CommonUtil.checkTextError(type,getString(R.string.Determine_the_category_of_use))||
                CommonUtil.checkEditError(password,getString(R.string.password))||
                CommonUtil.checkLength(password,getString(R.string.password_length),6))
            {
                return@setOnClickListener

        }else{
            if (!terms.isChecked){
                CommonUtil.makeToast(mContext,getString(R.string.check_terms))
            }else{
                if (phone.text.toString().length==9){
                    num = "0"+phone.text.toString()
                }else{
                    num = phone.text.toString()
                }
                signUp(num)
            }
        }
        }
        type.setOnClickListener {
            if (types.visibility ==View.VISIBLE){
                types.visibility = View.GONE
            }else{
                types.visibility = View.VISIBLE
            }
        }
        car.setOnClickListener { type.text = getString(R.string.car_wash)
        types.visibility = View.GONE
        Type = "wash"
        }
        oil.setOnClickListener { type.text = getString(R.string.Change_oil)
        types.visibility = View.GONE
        Type = "oil"}
        getKeys()
        code.setOnClickListener {
            if (codes.visibility==View.GONE){
                codes.visibility = View.VISIBLE
            }else{
                codes.visibility = View.GONE
            }
        }

    }

    fun getKeys(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Keys(mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<ListResponse>{
            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        listAdapter.updateAll(response.body()?.data!!)
                    }else{
                        codes.visibility = View.GONE
                    }
                }
            }
        })
    }
    fun signUp(num:String){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.SignUp("provider",name.text.toString(),num,code.text.toString(),emai.text.toString()
        ,password.text.toString(),deviceID,"android",Type,mLanguagePrefManager.appLanguage)?.enqueue(object :Callback<UserResponse>{
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful) {
                    if (response.body()?.value.equals("1")) {
                        val intent =
                            Intent(this@RegisterActivity, ActivateAccountActivity::class.java)
                        intent.putExtra("user", response.body()?.data)
                        startActivity(intent)
                    } else {
                        CommonUtil.makeToast(mContext, response.body()?.msg!!)
                    }
                }
            }
        })
    }
}