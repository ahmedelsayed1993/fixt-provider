package com.aait.fixtprovider.UI.Activities

import android.content.Intent
import android.os.Handler
import android.widget.ImageView
import com.aait.fixtprovider.Base.Parent_Activity
import com.aait.fixtprovider.R
import com.bumptech.glide.Glide
import me.ibrahimsn.lib.SmoothBottomBar

class SplashActivity : Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_splash
    private val SPLASH_DISPLAY_LENGTH = 5000
    lateinit var gif: ImageView
    override fun initializeComponents() {
        gif = findViewById(R.id.gif)
        Glide.with(mContext).load(R.raw.fixtlogo).into(gif)
        Handler().postDelayed(Runnable {
            if (mSharedPrefManager.loginStatus!!) {
                val mainIntent = Intent(this@SplashActivity, MainActivity::class.java)
                startActivity(mainIntent)
                finish()
            }else{
                val mainIntent = Intent(this@SplashActivity, LoginActivity::class.java)
                startActivity(mainIntent)
                finish()
            }
        }, SPLASH_DISPLAY_LENGTH.toLong())

    }


}
