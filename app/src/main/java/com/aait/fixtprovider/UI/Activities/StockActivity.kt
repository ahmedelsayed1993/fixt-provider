package com.aait.fixtprovider.UI.Activities

import android.content.Intent
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.fixtprovider.Base.Parent_Activity
import com.aait.fixtprovider.Client
import com.aait.fixtprovider.Models.FilterModel
import com.aait.fixtprovider.Models.OilModel
import com.aait.fixtprovider.Models.StockResponse
import com.aait.fixtprovider.Network.Service
import com.aait.fixtprovider.R
import com.aait.fixtprovider.UI.Adapters.FilterAdapter
import com.aait.fixtprovider.UI.Adapters.OilAdapter
import com.aait.fixtprovider.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class StockActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_stock
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var oil_recycler:RecyclerView
    lateinit var oil_filter_recycler:RecyclerView
    lateinit var makena_filter_recycler:RecyclerView
    lateinit var air_filter_recycler:RecyclerView
    var oilModels = ArrayList<OilModel>()
    var oilfilterModels = ArrayList<FilterModel>()
    var makenafilterModels = ArrayList<FilterModel>()
    var airfilterModels = ArrayList<FilterModel>()
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var linearLayoutManager1: LinearLayoutManager
    lateinit var linearLayoutManager2: LinearLayoutManager
    lateinit var linearLayoutManager3: LinearLayoutManager
    lateinit var oilAdapter: OilAdapter
    lateinit var filterAdapter: FilterAdapter
    lateinit var filterAdapter1: FilterAdapter
    lateinit var filterAdapter2: FilterAdapter
    lateinit var no_air:TextView
    lateinit var no_makena:TextView
    lateinit var no_oil:TextView
    lateinit var air_filter_lay:LinearLayout
    lateinit var makena_filter_lay:LinearLayout
    lateinit var oil_filter_lay:LinearLayout


    override fun initializeComponents() {
       back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        oil_recycler = findViewById(R.id.oil_recycler)
        oil_filter_recycler = findViewById(R.id.oil_filters_recycler)
        makena_filter_recycler = findViewById(R.id.makena_filters_recycler)
        air_filter_recycler = findViewById(R.id.air_filters_recycler)
        no_air = findViewById(R.id.no_air)
        no_makena = findViewById(R.id.no_makena)
        no_oil = findViewById(R.id.no_oil)
        air_filter_lay = findViewById(R.id.air_filter_lay)
        makena_filter_lay = findViewById(R.id.makena_filter_lay)
        oil_filter_lay = findViewById(R.id.oil_filter_lay)
        back.setOnClickListener { startActivity(Intent(this@StockActivity,MainActivity::class.java))
        finish()}
        title.text = getString(R.string.Inventory)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        linearLayoutManager1 = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        linearLayoutManager2 = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        linearLayoutManager3 = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        oilAdapter = OilAdapter(mContext,oilModels,R.layout.recycler_oil)
        filterAdapter = FilterAdapter(mContext,oilfilterModels,R.layout.recycler_filter)
        filterAdapter1 = FilterAdapter(mContext,makenafilterModels,R.layout.recycler_filter)
        filterAdapter2 = FilterAdapter(mContext,airfilterModels,R.layout.recycler_filter)
        oil_recycler.layoutManager = linearLayoutManager
        oil_recycler.adapter = oilAdapter
        oil_filter_recycler.layoutManager = linearLayoutManager1
        oil_filter_recycler.adapter = filterAdapter
        makena_filter_recycler.layoutManager = linearLayoutManager2
        makena_filter_recycler.adapter = filterAdapter1
        air_filter_recycler.layoutManager = linearLayoutManager3
        air_filter_recycler.adapter = filterAdapter2

        getStock()
    }

    fun getStock(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getStock(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!)
            ?.enqueue(object: Callback<StockResponse> {
                override fun onFailure(call: Call<StockResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<StockResponse>,
                    response: Response<StockResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            if (response.body()?.data?.isEmpty()!!){

                            }else{
                                oilAdapter.updateAll(response.body()?.data!!)
                            }
                            if (response.body()?.oil_filters?.isEmpty()!!){
                                  oil_filter_lay.visibility = View.GONE
                                no_oil.visibility = View.VISIBLE
                            }else{
                                oil_filter_lay.visibility = View.VISIBLE
                                no_oil.visibility = View.GONE
                                filterAdapter.updateAll(response.body()?.oil_filters!!)
                            }
                            if (response.body()?.makina_filters?.isEmpty()!!){
                                makena_filter_lay.visibility = View.GONE
                                no_makena.visibility = View.VISIBLE
                            }else{
                                makena_filter_lay.visibility = View.VISIBLE
                                no_makena.visibility = View.GONE
                                filterAdapter1.updateAll(response.body()?.makina_filters!!)
                            }
                            if (response.body()?.air_filters?.isEmpty()!!){
                                air_filter_lay.visibility = View.GONE
                                no_air.visibility = View.VISIBLE
                            }else{
                                air_filter_lay.visibility = View.VISIBLE
                                no_air.visibility = View.GONE
                                filterAdapter2.updateAll(response.body()?.air_filters!!)
                            }
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
    }
    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this,MainActivity::class.java))
    }
}