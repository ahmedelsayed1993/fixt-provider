package com.aait.fixtprovider.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.aait.fixtprovider.Base.ParentRecyclerAdapter
import com.aait.fixtprovider.Base.ParentRecyclerViewHolder
import com.aait.fixtprovider.Models.FilterModel
import com.aait.fixtprovider.Models.OilModel
import com.aait.fixtprovider.R

class FilterAdapter (context: Context, data: MutableList<FilterModel>, layoutId: Int) :
    ParentRecyclerAdapter<FilterModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }


    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val oilModel = data.get(position)

        viewHolder.quantity.text = oilModel.quantity_filter
        viewHolder.name.text = oilModel.filter_type
        viewHolder.car.text = oilModel.filter






    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {






        internal var quantity = itemView.findViewById<TextView>(R.id.quantity)
        internal var name = itemView.findViewById<TextView>(R.id.name)
        internal var car = itemView.findViewById<TextView>(R.id.car)



    }
}