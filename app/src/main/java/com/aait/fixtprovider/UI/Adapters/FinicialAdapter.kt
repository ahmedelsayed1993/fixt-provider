package com.aait.fixtprovider.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.aait.fixtprovider.Base.ParentRecyclerAdapter
import com.aait.fixtprovider.Base.ParentRecyclerViewHolder
import com.aait.fixtprovider.Models.FinacialModel
import com.aait.fixtprovider.Models.OilModel
import com.aait.fixtprovider.R

class FinicialAdapter (context: Context, data: MutableList<FinacialModel>, layoutId: Int) :
    ParentRecyclerAdapter<FinacialModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }


    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val finacialModel = data.get(position)
        viewHolder.order_num.text = finacialModel.order_id.toString()
        viewHolder.price.text = finacialModel.total_order
        viewHolder.commission.text = finacialModel.commission
        viewHolder.settlement.setOnClickListener(View.OnClickListener { view ->
            onItemClickListener.onItemClick(
                view,
                position
            ) })






    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var order_num=itemView.findViewById<TextView>(R.id.order_num)
        internal var price = itemView.findViewById<TextView>(R.id.price)
        internal var commission = itemView.findViewById<TextView>(R.id.commission)
        internal var settlement = itemView.findViewById<TextView>(R.id.settlement)



    }
}