package com.aait.fixtprovider.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.aait.fixtprovider.Base.ParentRecyclerAdapter
import com.aait.fixtprovider.Base.ParentRecyclerViewHolder
import com.aait.fixtprovider.Models.ListModel
import com.aait.fixtprovider.Models.VehicleModel
import com.aait.fixtprovider.R

class ListsAdapter (context: Context, data: MutableList<VehicleModel>, layoutId: Int) :
    ParentRecyclerAdapter<VehicleModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val vehicleModel = data.get(position)
        viewHolder.name!!.setText(vehicleModel.name)
        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position) })



    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var name=itemView.findViewById<TextView>(R.id.name)


    }
}