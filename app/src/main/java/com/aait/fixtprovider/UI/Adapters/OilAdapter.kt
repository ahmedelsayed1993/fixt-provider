package com.aait.fixtprovider.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.aait.fixtprovider.Base.ParentRecyclerAdapter
import com.aait.fixtprovider.Base.ParentRecyclerViewHolder
import com.aait.fixtprovider.Models.OilModel
import com.aait.fixtprovider.Models.OrderModel
import com.aait.fixtprovider.R
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView

class OilAdapter (context: Context, data: MutableList<OilModel>, layoutId: Int) :
    ParentRecyclerAdapter<OilModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }


    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val oilModel = data.get(position)
        viewHolder.m.text = oilModel.viscosity
        viewHolder.quantity.text = oilModel.quantity_oile
        viewHolder.name.text = oilModel.oil_type






    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var m=itemView.findViewById<TextView>(R.id.m)
        internal var quantity = itemView.findViewById<TextView>(R.id.quantity)
        internal var name = itemView.findViewById<TextView>(R.id.name)



    }
}