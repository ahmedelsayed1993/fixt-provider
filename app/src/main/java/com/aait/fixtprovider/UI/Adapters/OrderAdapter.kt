package com.aait.fixtprovider.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.TextView
import com.aait.fixtprovider.Base.ParentRecyclerAdapter
import com.aait.fixtprovider.Base.ParentRecyclerViewHolder

import com.aait.fixtprovider.Models.OrderModel
import com.aait.fixtprovider.R
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView

class OrderAdapter (context: Context, data: MutableList<OrderModel>, layoutId: Int) :
    ParentRecyclerAdapter<OrderModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }


    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val orderModel = data.get(position)

        viewHolder.time.text = orderModel.date+ " _ "+orderModel.time
        viewHolder.car.text = orderModel.car
        Glide.with(mcontext).load(orderModel.avatar).into(viewHolder.image)
        viewHolder.name.text = orderModel.name
        if (orderModel.order_type.equals("oil")){
            viewHolder.oil.visibility = View.VISIBLE
            viewHolder.wash.visibility = View.GONE
        }else if (orderModel.order_type.equals("wash")){
            viewHolder.oil.visibility = View.GONE
            viewHolder.wash.visibility = View.VISIBLE
        }
        if (orderModel.available_stock.equals("0")){
            viewHolder.purpose.visibility =View.GONE
        }else{
            viewHolder.purpose.visibility =View.VISIBLE
        }
        if (orderModel.oil_filter.equals("")){
            viewHolder.oil_filter.visibility =View.GONE
        }else{
            viewHolder.oil_filter.visibility =View.VISIBLE
        }
        if (orderModel.filter_air.equals("")){
            viewHolder.air_filter.visibility =View.GONE
        }else{
            viewHolder.air_filter.visibility =View.VISIBLE
        }
        if (orderModel.filter_makina.equals("")){
            viewHolder.makena_filter.visibility =View.GONE
        }else{
            viewHolder.makena_filter.visibility =View.VISIBLE
        }
        if (orderModel.payment_type.equals("")){
            viewHolder.cash.visibility =View.GONE
            viewHolder.credit.visibility = View.GONE
        }else{
            if (orderModel.payment_type.equals("cash")) {
                viewHolder.cash.visibility = View.VISIBLE
                viewHolder.credit.visibility = View.GONE
            }else if (orderModel.payment_type.equals("online")){
                viewHolder.cash.visibility = View.GONE
                viewHolder.credit.visibility = View.VISIBLE
            }
        }

        viewHolder.itemView.setOnClickListener(View.OnClickListener { view ->
            onItemClickListener.onItemClick(
                view,
                position
            ) })




    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var name=itemView.findViewById<TextView>(R.id.name)
        internal var image = itemView.findViewById<CircleImageView>(R.id.image)
        internal var car = itemView.findViewById<TextView>(R.id.car)
        internal var oil = itemView.findViewById<ImageView>(R.id.oil)
        internal var wash = itemView.findViewById<ImageView>(R.id.wash)
        internal var oil_filter = itemView.findViewById<ImageView>(R.id.oil_filter)
        internal var air_filter = itemView.findViewById<ImageView>(R.id.air_filter)
        internal var makena_filter = itemView.findViewById<ImageView>(R.id.makena_filter)
        internal var cash = itemView.findViewById<ImageView>(R.id.cash)
        internal var credit = itemView.findViewById<ImageView>(R.id.cerdit)
        internal var purpose = itemView.findViewById<TextView>(R.id.purposes)
        internal var time = itemView.findViewById<TextView>(R.id.time)


    }
}