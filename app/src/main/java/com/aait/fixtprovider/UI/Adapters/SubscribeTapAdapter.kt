package com.aait.fixtprovider.UI.Adapters

import android.content.Context

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter


import com.aait.fixtprovider.R
import com.aait.fixtprovider.UI.Fragments.CompletedFragment
import com.aait.fixtprovider.UI.Fragments.CurrentFragment


class SubscribeTapAdapter(
    private val context: Context,
    fm: FragmentManager
) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return if (position == 0) {
            CurrentFragment()
        } else {
            CompletedFragment()
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (position == 0) {
            context.getString(R.string.current)
        } else {
            context.getString(R.string.finished)
        }
    }
}
