package com.aait.fixtprovider.UI.Fragments

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.fixtprovider.Base.BaseFragment
import com.aait.fixtprovider.Client
import com.aait.fixtprovider.Listeners.OnItemClickListener
import com.aait.fixtprovider.Models.OrderModel
import com.aait.fixtprovider.Models.OrderResponse
import com.aait.fixtprovider.Network.Service
import com.aait.fixtprovider.R
import com.aait.fixtprovider.UI.Activities.EditProfileActivity
import com.aait.fixtprovider.UI.Activities.OrderDetailsActivity
import com.aait.fixtprovider.UI.Adapters.OrderAdapter
import com.aait.fixtprovider.Uitls.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeFragment:BaseFragment(),OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(activity,OrderDetailsActivity::class.java)
        intent.putExtra("id",orderModels.get(position).id)
        startActivity(intent)
    }

    override val layoutResource: Int
        get() = R.layout.fragment_home
    lateinit var edit:Button
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null
    lateinit var no:LinearLayout
    lateinit var lay:LinearLayout
    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    internal lateinit var linearLayoutManager: LinearLayoutManager
    internal var orderModels = java.util.ArrayList<OrderModel>()
    internal lateinit var orderAdapter: OrderAdapter

    override fun initializeComponents(view: View) {
        no = view.findViewById(R.id.no)
        lay = view.findViewById(R.id.lay)
        edit = view.findViewById(R.id.edit)
        edit.setOnClickListener { startActivity(Intent(activity,EditProfileActivity::class.java)) }
        rv_recycle = view.findViewById(R.id.rv_recycle)
        layNoInternet = view.findViewById(R.id.lay_no_internet)
        layNoItem = view.findViewById(R.id.lay_no_item)
        tvNoContent = view.findViewById(R.id.tv_no_content)
        swipeRefresh = view.findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        orderAdapter = OrderAdapter(mContext!!,orderModels,R.layout.recycler_orders)
        orderAdapter.setOnItemClickListener(this)
        rv_recycle!!.layoutManager= linearLayoutManager
        rv_recycle!!.adapter = orderAdapter
        no.visibility = View.GONE
        lay.visibility = View.GONE
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener { getHome() }
        getHome()

    }

    fun getHome(){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.getOrders(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,"current")?.enqueue(object :
            Callback<OrderResponse> {
            override fun onResponse(call: Call<OrderResponse>, response: Response<OrderResponse>) {
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful) {
                    try {
                        if (response.body()?.value.equals("1")) {
                            lay.visibility = View.VISIBLE
                            no.visibility = View.GONE
                            Log.e("myJobs", Gson().toJson(response.body()!!.data))
                            if (response.body()!!.data?.isEmpty()!!) {
                                layNoItem!!.visibility = View.VISIBLE
                                layNoInternet!!.visibility = View.GONE
                                tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                            } else {
//                            initSliderAds(response.body()?.slider!!)
                                orderAdapter.updateAll(response.body()!!.data!!)
                            }
//                            response.body()!!.data?.let { homeSeekerAdapter.updateAll(it)
                        }else if(response.body()?.value.equals("2")){lay.visibility = View.GONE
                            no.visibility = View.VISIBLE
                             CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                        }else{
                             CommonUtil.makeToast(mContext!!,response.body()?.msg!!)

                        }

                    } catch (e: Exception) {e.printStackTrace() }

                } else {  }
            }
            override fun onFailure(call: Call<OrderResponse>, t: Throwable) {
                 CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                Log.e("response", Gson().toJson(t))
            }
        })
    }
}