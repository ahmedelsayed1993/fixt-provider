package com.aait.fixtprovider.UI.Fragments

import android.view.View
import androidx.viewpager.widget.ViewPager
import com.aait.fixtprovider.Base.BaseFragment
import com.aait.fixtprovider.R
import com.aait.fixtprovider.UI.Adapters.SubscribeTapAdapter
import com.google.android.material.tabs.TabLayout

class OrderFragment :BaseFragment(){
    override val layoutResource: Int
        get() = R.layout.fragment_order
    lateinit var orders: TabLayout
    lateinit var ordersViewPager: ViewPager
    private var mAdapter: SubscribeTapAdapter? = null
    override fun initializeComponents(view: View) {
        orders = view.findViewById(R.id.orders)
        ordersViewPager = view.findViewById(R.id.ordersViewPager)
        mAdapter =  SubscribeTapAdapter(mContext!!,childFragmentManager)
        ordersViewPager.setAdapter(mAdapter)
        orders!!.setupWithViewPager(ordersViewPager)

    }
}