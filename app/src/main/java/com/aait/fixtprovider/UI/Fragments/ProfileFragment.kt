package com.aait.fixtprovider.UI.Fragments

import android.content.Intent
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.aait.fixtprovider.Base.BaseFragment
import com.aait.fixtprovider.Client
import com.aait.fixtprovider.Models.UserModel
import com.aait.fixtprovider.Models.UserResponse
import com.aait.fixtprovider.Network.Service
import com.aait.fixtprovider.R
import com.aait.fixtprovider.UI.Activities.EditDataActivity
import com.aait.fixtprovider.Uitls.CommonUtil
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProfileFragment:BaseFragment() {
    override val layoutResource: Int
        get() = R.layout.fragment_profile

    lateinit var image: CircleImageView
    lateinit var name: TextView
    lateinit var user_name: TextView
    lateinit var phone: TextView
    lateinit var email: TextView
    lateinit var edit: ImageView

    override fun initializeComponents(view: View) {
        image = view.findViewById(R.id.image)
        name = view.findViewById(R.id.name)
        user_name = view.findViewById(R.id.user_name)
        phone = view.findViewById(R.id.phone)
        email = view.findViewById(R.id.email)
        edit = view.findViewById(R.id.edit)

        getProfile()
        edit.setOnClickListener { startActivity(Intent(activity,EditDataActivity::class.java)) }
    }

    fun setData(userModel: UserModel){
         Glide.with(mContext!!).load(userModel.avatar).into(image)
        name.text = userModel.name
        user_name.text = userModel.name
        phone.text = userModel.phone
        email.text = userModel.email

    }

    fun getProfile(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.editProfile(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!
            ,null,null,null)?.enqueue(object : Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                 CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        setData(response.body()?.data!!)
                        mSharedPrefManager.userData = response.body()?.data!!
                    }else{
                         CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}